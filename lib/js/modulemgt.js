ModuleMgt = function() {};

ModuleMgt.LoadModule = function(module_name)
{
    $("#paneModuleArea").empty();
    
    $.ajax({
        url: "module-loader.php",
        method: "post",
        data: "loadmdl=true&modname="+module_name,
        success: function(data) 
        {
            if(data != "err")
            {
                $("#paneModuleArea").html(data);
            }
            else
            {
                $("#paneModuleArea").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Gerekli modülün yüklenmesi esnasında bir hata oluştu</div>');
            }
        },
       error: function()
       {
           $("#paneModuleArea").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Gerekli modülün yüklenmesi esnasında bir hata oluştu</div>');
       }
    });
};