function Util() { };

Util.isNumber = function(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

Util.mask = function(text, mask)
{
    var cur = 0;
    var result= "";
    for(var i = 0; i<mask.length; i++)
    {
        switch (mask.charAt(i))
        {
            case '#':
                for(var j = cur; j<text.length; i++)
                {
                    if(Util.isNumber(text.charAt(j)))
                    {
                        result += text.charAt(j);
                        cur = j+1;
                        break;
                    }
                }
            break;
            
            case '*':
                result+=text.charAt(i);
            break;
            
            default:
                result+=mask.charAt(i);
            break;
        }
    }
    return result;
};


Util.unmask = function(text, mask)
{
    var cur = 0;
    var result= "";
    for(var i = 0; i<mask.length; i++)
    {
        switch (mask.charAt(i))
        {
            case '#':
                for(var j = cur; j<text.length; i++)
                {
                    if(Util.isNumber(text.charAt(j)))
                    {
                        result += text.charAt(j);
                        cur = j+1;
                        break;
                    }
                }
            break;
            
            case '*':
                result+=text.charAt(i);
            break;
            
            default:
                cur++;
            break;
        }
    }
    return result;
};