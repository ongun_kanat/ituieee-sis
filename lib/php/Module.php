<?php
namespace ituieee\lib;
use PDOException;
use Exception;

if(!defined("__MAINDIR__"))
    define("__MAINDIR__","../../");

if(!defined("__MODULEDIR__"))
    define("__MODULEDIR__",__MAINDIR__."modules/");

/**
 * Modüller için temel altyapı sınıfı. Her sınıf permissionCheckOk()'yi uygulamak zorundadır.
 */
abstract class Module extends Page
{
    /**
     * @var \PDO $pdoDB
     */
    protected $pdoDB, $errlog, $name, $fields, $id, $shownName, $comment;
    protected $permited_fields = array();
    
    /**
     * /modules/$modulename/module.json dosyasını okur ve veritabanı kayıtlarını oluşturur
     * @param $modulename Modül adı. /modules/ISIM ile eşleşmelidir aksi halde
     * hata alınır.
     * @param $pdoObj PDO veritabanı sınıfının oluşturulmuş bir nesne örneği
     * @param $errlog ErrorLogger sınıfının oluşturulmuş bir nesne örneği
     */
    public function __construct($modulename, $pdoObj, $errlog) 
    {
        parent::__construct();
        $this->pdoDB = $pdoObj;
        $this->errlog = $errlog;
        
        if(!file_exists(__MODULEDIR__."$modulename/module.json") || !is_readable(__MODULEDIR__."$modulename/module.json"))
        {
            $errlog->insertErr("$modulename modülü açılamadı " . __MODULEDIR__."$modulename/module.json" . " dosyası yok veya okunabilir değil");
            throw new Exception("$modulename modülü yüklenemiyor");
        }
        $moduleInfo = json_decode(file_get_contents(__MODULEDIR__."$modulename/module.json"));
        if($moduleInfo == NULL)
        {
            $errlog->insertErr("$modulename e ait JSON hatalı");
            throw new Exception("$modulename modülü yüklenemiyor");
        }
        
        $this->name = $moduleInfo->isim;
        $this->fields = $moduleInfo->yetkiAlanlari;
        $this->shownName = $moduleInfo->gosterilenIsim;
        $this->comment = $moduleInfo->aciklama;
        $this->createTableFields();
    }
    
    /**
     * Veritabanında ilgili kayıtları oluşturan fonksiyon
     */
    private function createTableFields()
    {
        try
        {
            $moduleCheckQ = $this->pdoDB->prepare("SELECT * FROM moduller WHERE isim = :moduleName"); // Modülün kaydı DB'de var mı?
            $moduleCheckQ->execute(array("moduleName" => $this->name));
            $id;
            if($moduleCheckQ->rowCount() < 1) // Yoksa oluştur
            {
                $moduleInstertQ = $this->pdoDB->prepare("INSERT INTO moduller (id, isim, gosterilen_isim, uid, gid, aciklama) VALUES (:ID, :ISIM, :ALIAS, NULL, NULL, :ACIKLAMA)");
                $id =  DbUtil::idGenerate("moduller");
                $moduleInstertQ->execute(array("ID" => $id, "ISIM" => $this->name, "ALIAS" => $this->shownName, "ACIKLAMA" => $this->comment));
            }
            else // Varsa ve konfigurasyon değiştiyse verileri güncelle
            {
                $dbModule = $moduleCheckQ->fetchObject();
                $id = $dbModule->id;
                $changedItems = array();
                $changedValues = array();
                if($this->shownName != $dbModule->gosterilen_isim)
                {
                    array_push($changedItems, "gosterilen_isim = :ALIAS");
                    $changedValues["ALIAS"] = $this->shownName;
                }
                
                if($this->comment != $dbModule->aciklama)
                {
                    array_push($changedItems, "aciklama = :ACIKLAMA");
                    $changedValues["ACIKLAMA"] = $this->comment;
                }
                
                $changedItemsStr = implode(",", $changedItems);
                
                if(count($changedItems) > 0)
                {
                    $moduleUpdateQ = $this->pdoDB->prepare("UPDATE moduller SET $changedItemsStr WHERE id = :ID");
                    $changedValues["ID"] = $id;
                    $moduleUpdateQ->execute($changedValues);
                    
                }
                
            }
            $this->id = $id;
        }
        catch(PDOException $exc)
        {
            $this->errlog->insertErr($this->name . " adlı modül yaratılırken hata ile karşılaşıldı: " . $exc->getMessage()." Satır : " . $exc->getLine());
            throw new Exception($this->name . " modülü yüklenemiyor");
        }
        try
        {
            if(is_array($this->fields) ) // Yetki alanları kısmı
            {
                foreach ($this->fields as $currentfield)
                {
                    if(!is_a($currentfield, "stdClass") || is_null($currentfield->isim)|| is_null($currentfield->gosterilenIsim)|| is_null($currentfield->aciklama) ) // Yetki alanı bir sınıf mı?
                    {
                        $this->errlog->insertErr("$this->name adlı modüle ait field bilgisi hatalı veya eksik: " . print_r($currentfield, true) );
                        throw new Exception("$this->name modülü yüklenemiyor");
                    }
                    $fieldCheckQ = $this->pdoDB->prepare("SELECT * FROM yetkialanlari WHERE modul_id  = :MOD_ID AND isim = :ISIM"); // Yetki alanı DB'de kayıtlı mı
                    $fieldCheckQ->execute(array("MOD_ID" => $this->id, "ISIM" => $currentfield->isim));
                    
                    if($fieldCheckQ->rowCount() < 1) // Yoksa oluştur
                    {
                        $fieldInsertQ = null;
                        $dataArray = array();
                        if($currentfield->aciklama !=  "")
                        {
                            $fieldInsertQ = $this->pdoDB->prepare("INSERT INTO yetkialanlari VALUES (:ID, :ISIM, :ALIAS,:MOD_ID, NULL, NULL, :ACIKLAMA )");
                            $dataArray = array(
                            "ID" => DbUtil::idGenerate("yetkialanlari"),
                            "ISIM" => $currentfield->isim,
                            "ALIAS" => $currentfield->gosterilenIsim,
                            "MOD_ID" => $this->id,
                            "ACIKLAMA" => $currentfield->aciklama);
                        }
                        else 
                        {
                            $fieldInsertQ = $this->pdoDB->prepare("INSERT INTO yetkialanlari VALUES (:ID, :ISIM, :ALIAS,:MOD_ID, NULL, NULL, NULL)");
                            $dataArray = array(
                            "ID" => DbUtil::idGenerate("yetkialanlari"),
                            "ISIM" => $currentfield->isim,
                            "ALIAS" => $currentfield->gosterilenIsim,
                            "MOD_ID" => $this->id);
                        }
                        $fieldInsertQ->execute($dataArray);   
                    }
                    else // Varsa gerekli durumlarda güncelle
                    {
                        $dbField = $fieldCheckQ->fetchObject();
                        $id = $dbField->id;
                        $changedItems = array();
                        $changedValues = array();
                        if($currentfield->gosterilenIsim != $dbField->gosterilen_isim)
                        {
                            array_push($changedItems, "gosterilen_isim = :ALIAS");
                            $changedValues["ALIAS"] = $currentfield->gosterilenIsim;
                        }

                        if($currentfield->aciklama != $dbField->aciklama)
                        {
                            if($currentfield->aciklama == "")
                            {
                                array_push($changedItems, "aciklama = NULL");
                            }
                            else
                            {
                                array_push($changedItems, "aciklama = :ACIKLAMA");
                                $changedValues["ACIKLAMA"] = $currentfield->aciklama;
                            }
                        }

                        $changedItemsStr = implode(",", $changedItems);

                        if(count($changedItems) > 0)
                        {
                            $moduleUpdateQ = $this->pdoDB->prepare("UPDATE yetkialanlari SET $changedItemsStr WHERE id = :ID");
                            $changedValues["ID"] = $id;
                            $moduleUpdateQ->execute($changedValues);
                        }
                    }
                }
                $fieldListQ = $this->pdoDB->prepare("SELECT id,isim FROM yetkialanlari WHERE modul_id  = :MOD_ID"); // Silinmiş alanları veritabanından kaldır
                $fieldListQ->execute(array("MOD_ID" => $this->id));
                $dbFields = array();
                while( $currentfield = $fieldListQ->fetchObject() )
                {
                    array_push($dbFields, $currentfield);
                }
                $fieldNames = array();
                foreach ($this->fields as $currentfield)
                {
                    array_push($fieldNames, $currentfield->isim);
                }
                foreach ( $dbFields as $currentfield )
                {
                    if(!in_array($currentfield->isim, $fieldNames))
                    {
                        $fieldDeleteQ = $this->pdoDB->prepare("DELETE FROM yetkialanlari WHERE id = :ID");
                        $fieldDeleteQ->execute(array("ID" => $currentfield->id));
                    }
                }
                
            }
            else
            {
                $this->errlog->InsertErr("Modül sınıfında geçersiz permfields parametresi : ". print_r($this->fields, true));
                throw new Exception("$this->name modülü yüklenemiyor");
            }
        }
        catch(PDOException $exc)
        {
            $this->errlog->insertErr("$this->name adlı modüle ait fieldlar oluşturulamadı : " . $exc->getMessage()." Satır : " . $exc->getLine());
            throw new Exception("$this->name modülü yüklenemiyor");
        }
    }

    /**
     * Oturum açmış kullanıcının bu modülü kullanma yetkisi var mı ve hangi alanları kullanabilir
     * kontrol eder ve permitted_fields arrayini doldurur
     */
    private function pemissionCheck()
    {
        if($_SESSION["userid"] != "NULL")
        {
            try
            {
                $modulePermCheck = $this->pdoDB->prepare("SELECT id FROM moduller WHERE (gid = ANY (SELECT grup_id FROM grupuyelikleri WHERE uye_id = :uyeID) OR uid = :UID) AND id = :MOD_ID"); 
                $modulePermCheck->execute(array("uyeID" => $_SESSION["userid"], "UID" => $_SESSION["userid"], "MOD_ID" => $this->id));
                if($modulePermCheck->rowCount() > 0)
                {
                    $fieldPermCheckQ = $this->pdoDB->prepare("SELECT isim FROM yetkialanlari WHERE modul_id = :MOD_ID AND (gid = ANY (SELECT grup_id FROM grupuyelikleri WHERE uye_id = :uyeID) OR uid = :UID)");
                    $fieldPermCheckQ->execute(array(
                        "MOD_ID" => $this->id, 
                        "uyeID" => $_SESSION["userid"], 
                        "UID" => $_SESSION["userid"]));
                    while($currentField = $fieldPermCheckQ->fetchColumn())
                    {
                        array_push($this->permited_fields, $currentField);
                    }
                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(PDOException $exc)
            {
                $this->errlog->insertErr($this->name . "adlı modül için yetki bilgisi sınanamıyor : " . $exc->getMessage()." Satır : " . $exc->getLine());
                throw new Exception("$this->name");
            }
        }
        else
        {
            foreach ($this->fields as $currentField)
            {
                array_push($this->permited_fields, $currentField->isim);
            }
            return true;
        }
    }
    
    /*
     * Oturum bilgisinin devamında yetki kontrolü yap
     */
    protected function sessionCheckOk()
    {
        if($this->pemissionCheck())
        {
            $this->permissionCheckOk();
        }
        else
        {
            $this->permissionCheckFail();
        }
    }
    
    /*
     * Kullanıcının yetkisi varsa alt sınıfların uygulayacağı fonksiyon
     */
    abstract protected function permissionCheckOk();
    
    /*
     * Kullanıcının yetkisi yoksa hata mesajı görüntüle
     */
    protected function permissionCheckFail()
    {
?>
        <div class="alert alert-danger" id="alertModuleErr"><span class="glyphicon glyphicon-warning-sign"></span> Bu modülü yüklemek için yetkiniz yok</div>
<?php
    }
    
    
}