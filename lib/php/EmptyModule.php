<?php
namespace ituieee\lib;
use Exception;

class EmptyModule extends Module
{
    public function __construct($modulename, $pdoObj, $errlog) {
        parent::__construct($modulename, $pdoObj, $errlog);
        $this->errlog->insertInfo("EmptyModule $modulename adıyla oluşturuldu");
    }
    
    protected function permissionCheckOk() { } 
}