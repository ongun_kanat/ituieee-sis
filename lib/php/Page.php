<?php
namespace ituieee\lib;

/**
 * Sayfalar için iskelet sınıf
 */
abstract class Page
{
    protected $sessionStatus;
    
    public function __construct()
    {
        $this->sessionCheck();
    }
    
    /**
     * Başlık kısmının çizimi için iskelet metod
     */
    public function DrawHeader() {}
    
    /**
     * Altbilgi kısmının çizimi için iskelet metod
     */
    public function DrawFooter() {}
    
    /**
     * Gövde kısmının çizimi için iskelet metod
     */
    public function DrawBody() {}
    
    /**
     * Oturum bilgisi kontrolü (Login) başarılı olursa çalıştırılacak metod.
     * Bu metod tüm alt sınıflarca implement edilmelidir.
     */
    protected abstract function sessionCheckOk();
    
    /**
     * Oturum bilgisi kontrolü (Login) başarısız olursa çalıştırılacak metod
     * Bu metod varsayılan olarak index.php sayfasını yükler. Başka bir işlem
     * yapmak isteyen alt sınıflar bu metodu override etmelidir.
     */
    protected function sessionCheckFail() 
    {
        echo "<script>window.location.href='index.php'</script>";
    }
    
    /**
     * Oturum kontrol fonksiyonu, varsayılan olarak logged_in oturum değişkeni
     * kontrol edilir sonuç sessionStatus değişkenine yazılır. Alt sınıflar
     * gerekli durumlarda bu metodu override edebilir.
     */
    protected function sessionCheck()
    {
        if($_SESSION['logged_in'] == "true" )
        {
            $this->sessionStatus = true;
        }
        else
        {
            $this->sessionStatus = false;
        }
    }
    
    /**
     * Oturum bilgisine hassas içeriklerin (Login gerektiren) çizimini tetikleyen
     * metod. Varsayılan olarak sessionCheckOk veya sessionCheckFail fonksiyonları
     * çalıştırılır.
     */
    protected function DrawSessionSensetiveParts()
    {
        if($this->sessionStatus)
        {
            $this->sessionCheckOk();
        }
        else
        {
            $this->sessionCheckFail();
        }
    }
}