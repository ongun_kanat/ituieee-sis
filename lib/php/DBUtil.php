<?php
namespace ituieee\lib;
use PDO, PDOException;
require_once __CONFDIR__."/db_config.php";

class DbUtil
{
    /* @var DbUtil::$pdoDB PDO */
    private static $pdoDB = null;
    
    public static function init($dbobj)
    {
        if($dbobj && is_a($dbobj, "PDO") && !empty($dbobj->getAttribute(PDO::ATTR_SERVER_INFO)))
        {
            /* @var $dbobj PDO */;
            DbUtil::$pdoDB = $dbobj;
        }
        else
        {
            $default_errlog->insertErr("DbUtil init işlemi başarısız gelen \$dbobj : ".  print_r($dbobj, true));
            throw new Exception("DbUtil init işlemi başarısız");
        }
    }
    
    private static function isInit()
    {
        if(DbUtil::$pdoDB === null)
            return false;
        else
            return true;
    }

    public static function idGenerate($table ,$digits = 8, $start = 0)
    {        
        if(DbUtil::isInit())
        {   try
            {
                if(DbUtil::$pdoDB->query("SELECT id FROM $table")->rowCount() >= pow(10, $digits) - $start)
                {
                    $default_errlog->insertErr("DbUtil ID üretimi esnasında hata: $table tablosundaki kayıt sayısı üretilebilir ID sayısını aşıyor." );
                    throw new Exception("DbUtil ID üretimi esnasında hata");
                }
            
                $id;
                do
                {
                    $id = rand($start, pow(10, $digits) - 1);
                }
                while(DbUtil::$pdoDB->query("SELECT id FROM $table WHERE id = $id")->rowCount() > 0);
                return $id;
            }
            catch (PDOException $exc)
            {
                $default_errlog->insertErr("DbUtil ID üretimi esnasında hata " . $exc->getMessage() );
                throw new Exception("DbUtil ID üretimi esnasında hata");
            }
        }
        else
        {
            throw new Exception("Init edilmemiş DbUtil kullanımı");
        }
    }
    
}