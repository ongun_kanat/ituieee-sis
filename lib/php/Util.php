<?php
namespace ituieee\lib;
class Util
{
    private static $token_key = "-=]=N-s[qW_aAxctdod:sdj@kfj'kkflks";
    public static function textMask($text, $mask)
    {
        $cur; $result = "";
        for($i = 0; $i < mb_strlen($mask); $i++)
        {
            $char = mb_substr($mask, $i, 1);
            switch ($char) {
                case "#":
                    for($j = $cur; $j < mb_strlen($text); $j++ )
                    {
                        if(is_numeric($metin = mb_substr($text, $cur, 1)))
                        {
                            $result .= $metin;
                            $cur++;
                            break;
                        }
                    }
                break;

                default:
                    $result .= $char;
                break;
            }
        }
        return $result;
    }
    
    public static function textUnMask($text, $mask)
    {
        $cur; $result = "";
        for($i = 0; $i < mb_strlen($mask); $i++)
        {
            $char = mb_substr($mask, $i, 1);
            switch ($char) {
                case "#":
                    for($j = $cur; $j < mb_strlen($text); $j++ )
                    {
                        if(is_numeric($metin = mb_substr($text, $cur, 1)))
                        {
                            $result .= $metin;
                            $cur++;
                            break;
                        }
                    }
                break;

                default:
                    $cur++;
                break;
            }
        }
        return $result;
    }
    
    public static function tokenGenerate($command,$values)
    {
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC), MCRYPT_RAND);
        $message = $command . "\xE2\x9A\xAB" . $values;
        $token = mcrypt_encrypt(MCRYPT_BLOWFISH, Util::$token_key, $message, MCRYPT_MODE_CBC, $iv).$iv;
        return base64_encode($token);
    }
    
    public static function tokenDecode($token)
    {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $token = base64_decode($token);
        $iv = substr($token, strlen($token)-$iv_size, $iv_size);
        $token = substr($token, 0, strlen($token)-$iv_size);
        $token = rtrim(mcrypt_decrypt(MCRYPT_BLOWFISH, Util::$token_key, $token, MCRYPT_MODE_CBC, $iv));
        $split_arr = explode("\xE2\x9A\xAB", $token);
        return array("command" => $split_arr[0], "values" => $split_arr[1]);
    }
    
    public static function jsonPrettyPrint($json) 
    {
     
        $result = '';
        $pos = 0;
        $strLen = strlen($json);
        $indentStr = ' ';
        $newLine = "\n";
        $prevChar = '';
        $outOfQuotes = true;

        for ($i = 0; $i <= $strLen; $i++) {

            // Grab the next character in the string.
            $char = substr($json, $i, 1);

            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;

                // If this character is the end of an element,
                // output a new line and indent the next line.
            } else if (($char == '}' || $char == ']') && $outOfQuotes) {
                $result .= $newLine;
                $pos --;
                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            // Add the character to the result string.
            $result .= $char;

            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos ++;
                }

                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }

        return $result;
    }

}