<?php
namespace ituieee\lib;
use Exception;

class ErrorLogger
{
    private $strm;
    private $open;
    private $debug;
    public function __construct($logf, $mainlogger = false, $debugmode = 1)
    {
        $this->strm = fopen($logf, "a+");
        if(!$this->strm)
        {
            $exc = new Exception ("Log dosyası açılamadı.");
            if($mainlogger)
            {
                die($exc->getMessage());
            }
            throw $exc;
        }
        else 
        { 
            $this->open = true;
            $this->debug = $debugmode;            
        }
    }
    
    public function insertErr($message)
    {
        $err_str = date("[D, Y-m-d H:i:s]")." ERR: $message \n";
        if(!fwrite($this->strm, $err_str))
        {
            $this->nonLogErr($err_str, new Exception($err_str) );
        }
    }
    
    public function nonLogErr($str, $prev_exception = null)
    {
        throw new Exception("Loglanamadı : $str", "0xffff", $prev_exception);
    }
    
    public function insertWarn($message)
    {
        if($this->debug > 0)
        {
            $err_str = date("[D, Y-m-d H:i:s]")." WARN: $message \n";
            if(!fwrite($this->strm, $err_str))
            {
                $this->nonLogErr($err_str);
            }
        }
    }
    
    public function insertInfo($message)
    {
        if($this->debug > 1)
        {
            $err_str = date("[D, Y-m-d H:i:s]")." INFO: $message \n";
            if(!fwrite($this->strm, $err_str))
            {
                $this->nonLogErr($err_str);
            }
        }
    }
    
    public function __destruct() 
    {
        fclose($this->strm);
    }
}