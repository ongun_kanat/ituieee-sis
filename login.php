<?php 
require_once "includes.php";
use ituieee\lib\Util;

define("ADMIN_USR", $GLOBALS["settings"]->admin_user);
define("ADMIN_PASS", $GLOBALS["settings"]->admin_password);

if($_POST["opcode"] != "")
{
    $opcode = Util::tokenDecode($_POST["opcode"]);
    if($opcode["command"] == "loginop" )
    {
        if((time() - strtotime($opcode["values"])) > 45)
        {
            die("3");
        }
        /* @var $pdoDB PDO */
        try
        {
            $sifreli = hash("sha256", $_POST["eposta"] . "/". $_POST["sifre"]);
            if($_POST["eposta"] == ADMIN_USR)
            {
                if($sifreli == ADMIN_PASS)
                {
                    $_SESSION['logged_in'] = "true";
                    $_SESSION['userid'] = "NULL";
                    $_SESSION['grup_id'] = "NULL";
                    echo "1";
                    require_once "module-finder.php";
                }
                else
                {
                    echo "0";
                }
            }
            else
            {
                $userCheck = $pdoDB->prepare("SELECT * FROM uyeler WHERE eposta = :eposta AND sifre = :sif");

                $userCheck->execute(array("eposta" => $_POST["eposta"], "sif" => $sifreli));
                if($userCheck->rowCount() > 0)
                {
                    $kullanici = $userCheck->fetch(PDO::FETCH_ASSOC);
                    $_SESSION['logged_in'] = "true";
                    $_SESSION['userid'] = $kullanici["id"];
                    $_SESSION['grup_id'] = $kullanici["grup_id"];
                    echo "1";
                    require_once "module-finder.php";
                }
                else 
                {
                    echo "0";
                }
            }
        }
        catch (PDOException $exception)
        {
            echo "2";
            $default_errlog->insertErr("Oturum açma işlemi esnasında hata: ". $exception->getMessage());
        }
        
    }
}
else
{
    $login_sec = explode("/", $_SERVER["PHP_SELF"]);
    if($login_sec[count($login_sec) - 1] != "index.php")
    {
        header("Content-type: text/html; Charset=UTF-8");
        die("Üye girişi yalnızca ana sayfadan çağırılabilir");
    }
?>
    <div class="container">
        <div class="col-xs-offset-3 col-xs-6">
            <div class="panel panel-default">
                <div class="panel-heading">Kullanıcı Girişi</div>
                <div class="panel-body">
                    <div class="alert alert-warning hidden" id="altUserErr"><span class="glyphicon glyphicon-warning-sign"></span> Girdiğiniz e-posta veya parola yanlış</div>
                    <div class="alert alert-danger hidden" id="altErr"><span class="glyphicon glyphicon-warning-sign"></span> Oturum açma işlemi esnasında bir hata oluştu</div>
                    <div class="alert alert-danger hidden" id="altValid"><span class="glyphicon glyphicon-warning-sign"></span> Lütfen geçerli bir e-posta adı ve şifre girin.</div>
                    <div class="alert alert-warning hidden" id="altTimeOut"><span class="glyphicon glyphicon-warning-sign"></span> Oturum açma süreniz doldu sayfayı yenilemek için <a class="alert-link" href="index.php">burayı tıklayın.</a></div>
                    <form role="form" class="form-horizontal" id="frmLogin" >
                        <div class="form-group">
                            <div class="col-sm-3"><label for="txtKullAdi">E-Posta Adresi</label></div>
                            <div class="col-sm-9"><input type="text" class="form-control" id="txtKullAdi" placeholder="E-Posta" name="eposta" autofocus /></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3"><label for="txtSifre">Şifre</label></div>
                            <div class="col-sm-9"><input type="password" class="form-control" id="txtSifre" placeholder="Şifre" name="sifre"/></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <a href="javascript:void(0)"><button type="button" class="btn btn-primary" id="btnLogin">Oturum Aç</button></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#txtKullAdi, #txtSifre").on("keyup", function(evt){
                if(evt.key == "Enter")
                    $("#btnLogin").trigger("click");
            });

            $("#btnLogin").on("click", function(){
                var ok = true;//Validasyon

                $(".alert").addClass("hidden");//Tüm uyarıları gizle

                $("#frmLogin input").each(function() { //Tüm girdiler için check işlemi
                    $(this).parent().removeClass("has-error");
                    $(this).parent().removeClass("has-warning");
                    if($(this).val() == "") //İçi boşsa hata
                    {
                        ok=false;
                        $(this).parent().addClass("has-error");
                        $("#altValid").removeClass("hidden");
                    }
                });
                if(ok){//Kutular doluysa ajax ile oturum aç
                    $.ajax({
                        url: "login.php", //TODO: htaccess gizlemeleri
                        method: "post",
                        data: $("#frmLogin").serialize()+"&opcode=<?= urlencode(Util::tokenGenerate("loginop", date("Y-m-d H:i:s"))) ?>",
                        success: function (data) 
                        {
                            switch(data)
                            {
                                case "1"://İşlem Başarılı
                                    window.location.href="index.php";
                                break;

                                case "0"://Kullanıcı adı ve parola yanlış
                                    $("#altUserErr").removeClass("hidden");
                                    $("#frmLogin input").each(function() { $(this).parent().addClass("has-warning")});
                                break;

                                case "3":
                                    $("#altTimeOut").removeClass("hidden");
                                break;

                                default://Hata
                                    $("#altErr").removeClass("hidden");      
                            }
                        }
                    });
                }
            });
        });

    </script>
<?php } ?>
