<?php
namespace ituieee\modules\Uyeler;

require_once 'includes.php';
use ituieee\lib\Util;
use PDO;
use PDOException;

class UyelerMod extends \ituieee\lib\Module
{
    public function __construct($pdoObj, $errlog) 
    {
        parent::__construct("Uyeler", $pdoObj, $errlog);
    }
    
    public function DrawBody()
    {
        $this->DrawSessionSensetiveParts();
    }
    
    protected function permissionCheckOk() 
    {
        require_once __MODULEDIR__."Uyeler/javascript.php";  
?>
        <div class="col-xs-12 alert hidden" id="alertArea"></div>
        <div class="container-fluid" id="listArea"></div>
<?php
        if(in_array("addUye", $this->permited_fields) ||  in_array("editUye", $this->permited_fields) ||  in_array("editSelfUye", $this->permited_fields))
        {
?>
            <div id="modalUyeEkleDuz" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-editmode="">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalTitleEkleDuz"></h4>
                        </div>
                        <div class="modal-body">

                            <form role="form" class="form-horizontal" id="frmUyeEkleDuz"  >
                                <div class="container-fluid">
                                    <div class="form-group">
                                        <div class="col-sm-3"><label for="txtModalUyeAdi" class="control-label">Üye Adı</label></div>
                                        <div class="col-sm-9"><input type="text" class="form-control" id="txtModalUyeAdi" placeholder="Üye Adı" name="uyeAdi" pattern="[A-ZÜĞİŞÇÖ][A-Za-zÜĞİŞÇÖüğışçö].+ [A-ZÜĞİŞÇÖ][A-Za-zÜĞİŞÇÖüğışçö].+" minlength="5" required /></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"><label for="txtModalUyeTelefon" class="control-label">Telefon</label></div>
                                        <div class="col-sm-9"><input type="tel" class="form-control" id="txtModalUyeTelefon" placeholder="Telefon Numarası" name="uyeTel" mask="(999) 999 9999" pattern="\([0-9]{3}\) [0-9]{3} [0-9]{4}" required /></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"><label for="txtModalUyeBolum" class="control-label">Bölüm</label></div>
                                        <div class="col-sm-9"><input type="tel" class="form-control" id="txtModalUyeBolum" placeholder="Bölüm" name="uyeBolum"  pattern="[A-ZÜĞİŞÇÖ][A-Za-zÜĞİŞÇÖüğışçö].+ [A-ZÜĞİŞÇÖ][A-Za-zÜĞİŞÇÖüğışçö].+" minlength="6" required/></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"><label for="fldModalUyeCinsiyet" class="control-label">Cinsiyet</label></div>
                                        <div class="col-sm-9">
                                            <fieldset id="fldModalUyeCinsiyet">
                                                <label class="radio-inline"><input type="radio" name="uyeCinsiyet" value="E" id="radModalUyeErkek" minchecked="1" />Erkek</label>
                                                <label class="radio-inline"><input type="radio" name="uyeCinsiyet" value="K" id="radModalUyeKadin" />Kadın</label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"><label for="txtModalUyeEposta" class="control-label">E-Posta</label></div>
                                        <div class="col-sm-9"><input type="email" class="form-control" id="txtModalUyeEposta" placeholder="Üye E-Posta" name="uyeEPosta" required /></div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-success hidden" id="btnModalUyeEkle" disabled>Ekle</button>
                            <button type="button" class="btn btn-primary hidden" id="btnModalUyeDuzenle" disabled>Bilgileri Kaydet</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">İptal</button>
                        </div>
                    </div>
                </div>
            </div>
<?php
        }
        
        if(in_array("deleteUye", $this->permited_fields))
        {
?>
            <div id="modalUyeSil" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Üye Sil</h4>
                        </div>
                        <div class="modal-body">
                            <span id="fldModalUyeAdi"></span> adlı üyeyi silmek istiyor musunuz? Bu işlem geri alınamaz
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" id="btnModalUyeSil">Sil</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">İptal</button>
                        </div>
                    </div>
                </div>
            </div>
<?php
        }
    }

}

$uyeler = new UyelerMod($pdoDB, $default_errlog);
$uyeler->DrawBody();