<?php
use ituieee\lib\Util;
?>
<script type="text/javascript">
    
        
    $(document).ready(function(ev)
    {
        $.ajaxSetup({
            url: "<?= __MODULEDIR__?>Uyeler/operations.php"
        });
        
        $("#listArea").data({
            LoadPage: function(pagenum, search, searchStr){
                search = typeof search !== "undefined" ? true : false;
                searchStr = typeof searchStr === "string" ? searchStr : null;
                
                var dataStr = "opcode=<?= urlencode(Util::tokenGenerate("list", date("Y-m-d H:i:s")))?>&pagenum="+pagenum;
                
                if(search && searchStr)
                {
                    dataStr = "opcode=<?= urlencode(Util::tokenGenerate("searchUyeList", date("Y-m-d H:i:s")))?>&pagenum="+pagenum+"&searchStr="+searchStr;
                    $("#listArea").attr("data-searchstr",searchStr);
                }
                else if ($("#listArea").attr("data-searchstr") && $("#txtAraStr").val() != "")
                {
                    dataStr = "opcode=<?= urlencode(Util::tokenGenerate("searchUyeList", date("Y-m-d H:i:s")))?>&pagenum="+pagenum+"&searchStr="+$("#listArea").attr("data-searchstr");
                }
                
                $.ajax({
                    
                    method: "post",
                    data: dataStr,
                    success: function (data)
                    {
                        $("#listArea").html(data);
                        $("#btnYenile").attr("data-page", pagenum);
                        RefreshEvents();
                    }
                });
            }
        });
        
        function ShowAlertErr(message)
        {
            $("#alertArea").removeClass("alert-danger").removeClass("alert-warning");
            $("#alertArea").removeClass("hidden").addClass("alert-danger").html(message);
            window.setTimeout(function(){ $("#alertArea").addClass("hidden");  }, 4000);
            $("#listArea").data('LoadPage')($("#btnYenile").attr("data-page"));
        }
        
        function ShowAlertWarn(message)
        {
            $("#alertArea").removeClass("alert-danger").removeClass("alert-warning");
            $("#alertArea").removeClass("hidden").addClass("alert").addClass("alert-warning").html(message);
            window.setTimeout(function(){ $("#alertArea").addClass("hidden"); }, 4000);
            $("#listArea").data('LoadPage')($("#btnYenile").attr("data-page"));
        }
        
<?php
        if(in_array("addUye", $this->permited_fields) ||  in_array("editUye", $this->permited_fields) ||  in_array("editSelfUye", $this->permited_fields) )
        {
?>     
            $("#frmUyeEkleDuz input").jqBootstrapValidation().on("blur focus change keyup",function(event_data){
                if(!$("#frmUyeEkleDuz input").jqBootstrapValidation("hasErrors"))
                {
                    $("#btnModalUyeEkle").removeAttr("disabled");

                    var uyeDuzenlendi = false;
                    $("#frmUyeEkleDuz input").each(function(){
                        if($(this).attr("data-prevval") != $(this).val())
                            uyeDuzenlendi = true;
                    });

                    if(uyeDuzenlendi)
                        $("#btnModalUyeDuzenle").removeAttr("disabled");
                    else
                       $("#btnModalUyeDuzenle").attr("disabled",""); 
                }
                else
                {
                    $("#btnModalUyeEkle").attr("disabled","");
                    $("#btnModalUyeDuzenle").attr("disabled","");
                }
            });
        
            $("#modalUyeEkleDuz").on("show.bs.modal",function(event_data)
            {
                var job = $(event_data.relatedTarget).attr("data-job");
                $(this).attr("data-jobtype", job);

                if(job == "addUye")
                {
                    $("#modalTitleEkleDuz").html("Üye Ekle");
                    $("#btnModalUyeEkle").removeClass("hidden");
                }
                else if(job == "editUye")
                {
                    $("#modalTitleEkleDuz").html("Üye Düzenle");
                    $("#btnModalUyeDuzenle").removeClass("hidden");
                    var uyeId = $(event_data.relatedTarget).parents("tr").attr("data-uyeid");
                    $("#btnModalUyeDuzenle").attr("data-uyeID", uyeId);
                    $.ajax({

                        method: "post",
                        data: "uyeId=" + uyeId + "&opcode=<?= urlencode(Util::tokenGenerate("getUyeInfo", date("Y-m-d H:i:s"))); ?>",
                        dataType: "json",
                        success: function(data){
                            if(data.status == "success")
                            {
                                $("#txtModalUyeAdi").val(data.isim);
                                $("#txtModalUyeTelefon").val(Util.mask(data.telefon,"(###) ### ####"));
                                $("#txtModalUyeBolum").val(data.bolum);
                                if(data.cinsiyet == "E")
                                    $("#radModalUyeErkek").prop("checked", true);
                                else
                                    $("#radModalUyeKadin").prop("checked", true);
                                $("#txtModalUyeEposta").val(data.eposta);
                                $("#txtModalUyeAdi").attr("data-prevval", data.isim);
                                $("#txtModalUyeTelefon").attr("data-prevval", Util.mask(data.telefon,"(###) ### ####"));
                                $("#txtModalUyeEposta").attr("data-prevval", data.eposta);
                            }
                            else if( data.status == "nosuchUye" )
                            {
                                ShowAlertWarn("Seçtiğiniz üye ne yazık ki bulunamadı. Veritabanında kaldırılmış olabilir. Yeni Üye Listesi için listeyi yenileyebilirsiniz.");
                                $("#modalUyeEkleDuz").modal('hide');
                            }
                            else
                            {
                                ShowAlertErr("Üye düzenleme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                                $("#modalUyeEkleDuz").modal('hide');
                            }
                        },
                        error: function(){
                            ShowAlertErr("Üye düzenleme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                            $("#modalUyeEkleDuz").modal('hide');
                        }
                    });
                }
            });
        
            $("#modalUyeEkleDuz").on("hide.bs.modal",function(event_data)
            {
                $("#frmUyeEkleDuz .form-group").removeClass("has-success").removeClass("has-warning").removeClass("has-error");
                $("#frmUyeEkleDuz input:not([type=radio])").val("");
                $("#frmUyeEkleDuz input[type=radio]").prop("checked", false);
                $("#btnModalUyeEkle").attr("disabled","").addClass("hidden");
                $("#btnModalUyeDuzenle").attr("disabled","").addClass("hidden");
                $("#btnModalUyeDuzenle").removeAttr("data-uyeID");
            });
        
            $("#btnModalUyeEkle").click(function(click_evt){
                $.ajax({

                    method: "post",
                    data: $("#frmUyeEkleDuz").serialize() +"&opcode=<?= urlencode(Util::tokenGenerate("adduye", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            $("#listArea").data("LoadPage")(data.page);
                        }
                        else
                        {
                            ShowAlertErr("Üye ekleme işlemi sırasında bir hata oluştu");
                        }
                        $("#modalUyeEkleDuz").modal('hide');
                    },
                    error: function(){
                        ShowAlertErr("Üye ekleme işlemi sırasında bir hata oluştu");
                        $("#modalUyeEkleDuz").modal('hide');
                    }
                });
            });
        
            $("#btnModalUyeDuzenle").click(function(click_evt){
                var uyeID = $("#btnModalUyeDuzenle").attr("data-uyeID");
                $.ajax({

                    method: "post",
                    data: $("#frmUyeEkleDuz").serialize() + "&uyeId=" + uyeID + "&opcode=<?= urlencode(Util::tokenGenerate("editUye", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            $("#listArea").data("LoadPage")(data.page);
                        }
                        else
                        {
                            ShowAlertErr("Üye Düzenleme esnasında bir hata ile karşılaşıldı");
                        }
                        $("#modalUyeEkleDuz").modal('hide');
                    },
                    error: function(){
                        ShowAlertErr("Üye düzenleme işlemi sırasında bir hata oluştu");
                        $("#modalUyeEkleDuz").modal('hide');
                    }
                });
            });
        
<?php
        }
        if(in_array("deleteUye", $this->permited_fields))
        {
?>
        
            $("#modalUyeSil").on("show.bs.modal",function(event_data)
            {
                var uyeId = $(event_data.relatedTarget).parents("tr").attr("data-uyeid");
                $.ajax({

                    method: "post",
                    data: "uyeId=" + uyeId + "&opcode=<?= urlencode(Util::tokenGenerate("getUyeInfo", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            $("#fldModalUyeAdi").html(data.isim);
                            $("#btnModalUyeSil").attr("data-uyeid", uyeId);
                        }
                        else if( data.status == "nosuchUye" )
                        {
                            ShowAlertWarn("Seçtiğiniz üye bulunamadı. Veritabanında kaldırılmış olabilir. Yeni Üye Listesi için listeyi üstteki düğme yardımıyla yenileyebilirsiniz.");
                            $("#modalUyeSil").modal('hide');
                        }
                        else
                        {
                            ShowAlertErr("Üye silme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                            $("#modalUyeSil").modal('hide');
                        }
                    },
                    error: function(){
                        ShowAlertErr("Üye silme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                        $("#modalUyeEkleDuz").modal('hide');
                    }
                });
            });
        
            $("#modalUyeSil").on("hide.bs.modal",function(event_data)
            {
                $("#fldModalUyeAdi").html("");
                $("#btnModalUyeSil").removeAttr("data-uyeid");
            });
        
            $("#btnModalUyeSil").click(function(evt){
               var uyeId = $(this).attr("data-uyeid");
               $.ajax({

                    method: "post",
                    data: "uyeId=" + uyeId + "&opcode=<?= urlencode(Util::tokenGenerate("deleteUye", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            $("#listArea").data("LoadPage")(data.page);
                        }
                        else
                        {
                            ShowAlertErr("Üye silme işlemi esnasında bir hata ile karşılaşıldı!");
                        }
                        $("#modalUyeSil").modal('hide');
                    },
                    error: function(){
                        ShowAlertErr("Üye silme işlemi sırasında bir hata oluştu");
                        $("#modalUyeSil").modal('hide');
                    }
                }); 
            });
<?php
        }
?>        
        $("input[mask]").each(function()
        {
            $(this).setMask($(this).attr("mask"));
        });
        
        $("#listArea").data("LoadPage")(0);
    });
    
    
</script>