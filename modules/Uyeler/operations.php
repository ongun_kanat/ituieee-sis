<?php
namespace ituieee\modules\Uyeler;
require_once "includes.php";
use ituieee\lib\Util;
use ituieee\lib\DbUtil;
use PDO;
use PDOException;

class UyelerOps extends \ituieee\lib\Module
{
    private $uyePerPage = 10;
    public function __construct($pdoObj, $errlog) 
    {
        parent::__construct("Uyeler", $pdoObj, $errlog);
        $this->DrawSessionSensetiveParts();
    }

    protected function permissionCheckOk()
    {
        if($_POST["opcode"] != "" )
        {
            $opcode = Util::tokenDecode($_POST["opcode"]);
            if($opcode["command"] == "list")
            {
                header("Content-type = text/html; Charset=UTF-8");
                $this->listUye($_POST["pagenum"]);
            }
            elseif($opcode["command"] == "adduye")
            {
                header("Content-type: application/json; charset=UTF-8");
                if(in_array("addUye", $this->permited_fields))
                {
                    try
                    {
                        $uyeAddSQL = "";
                        $uyeAddExecuteArr = array();
                        $uyeID =DbUtil::idGenerate("uyeler");
                        
                        if($_SESSION["userid"] != "NULL")
                        {
                            $uyeAddSQL = "INSERT INTO uyeler VALUES (:uyeId, :uyeAdi, :uyeBolum, :uyeCinsiyet, :uyeEposta, :uyeTel, NULL, NULL, :eklID, :eklTarih, :duzTarih, :eklDon, :aktDon)";
                            $uyeAddExecuteArr = array("uyeId"   => $uyeID , 
                                                "uyeAdi"        => $_POST["uyeAdi"],
                                                "uyeBolum"      => $_POST["uyeBolum"],
                                                "uyeCinsiyet"   => $_POST["uyeCinsiyet"], 
                                                "uyeEposta"     => $_POST["uyeEPosta"], 
                                                "uyeTel"        => Util::textUnMask($_POST["uyeTel"], "(###) ### ####"),
                                                "eklID"         => $_SESSION["userid"],
                                                "eklTarih"      => date("Y-m-d H:i:s"),
                                                "duzTarih"      => date("Y-m-d H:i:s"), 
                                                "eklDon"        => $GLOBALS['settings']->aktif_donem,
                                                "aktDon"        => $GLOBALS['settings']->aktif_donem );
                        }
                        else
                        {
                            $uyeAddSQL = "INSERT INTO uyeler VALUES (:uyeId, :uyeAdi, :uyeBolum, :uyeCinsiyet, :uyeEposta, :uyeTel, NULL, NULL, NULL, :eklTarih, :duzTarih, :eklDon, :aktDon)";
                            $uyeAddExecuteArr = array("uyeId"   => $uyeID , 
                                                "uyeAdi"        => $_POST["uyeAdi"],
                                                "uyeBolum"      => $_POST["uyeBolum"],
                                                "uyeCinsiyet"   => $_POST["uyeCinsiyet"], 
                                                "uyeEposta"     => $_POST["uyeEPosta"], 
                                                "uyeTel"        => Util::textUnMask($_POST["uyeTel"], "(###) ### ####"),
                                                "eklTarih"      => date("Y-m-d H:i:s"),
                                                "duzTarih"      => date("Y-m-d H:i:s"), 
                                                "eklDon"        => $GLOBALS['settings']->aktif_donem,
                                                "aktDon"        => $GLOBALS['settings']->aktif_donem );
                        }
                        
                        $uyeAddQ = $this->pdoDB->prepare($uyeAddSQL);
                        $uyeAddQ->execute($uyeAddExecuteArr);
                        $pageN = $this->pdoDB->query("SELECT FLOOR(COUNT(*)/ $this->uyePerPage) as sayfa_no FROM uyeler WHERE id < $uyeID ORDER BY id")->fetchColumn();
                        echo Util::jsonPrettyPrint("{\"status\": \"success\", \"page\": $pageN }");
                    }
                    catch(PDOException $exc)
                    {
                        $this->errlog->insertErr("Üye ekleme sırasında hata oluştu : ". $exc->getMessage());
                        echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                    }
                }
                else
                {
                    $this->errlog->insertWarn("Üyeler modülünde yetkisiz ekleme isteği yapıldı");
                    echo Util::jsonPrettyPrint("{\"status\": \"notpermit\"}");
                }
            }
            elseif($opcode["command"] == "getUyeInfo")
            {
                header("Content-type: application/json; charset=UTF-8");
                try
                {
                    $uyeDecode = Util::tokenDecode($_POST["uyeId"]);
                    $uyeID = $uyeDecode["values"];
                    $uyeQ = $this->pdoDB->prepare("SELECT isim, cinsiyet, bolum, eposta, telefon FROM uyeler WHERE id = :uyeId");
                    $uyeQ->execute(array("uyeId" => $uyeID));
                    if($uyeQ->rowCount() > 0)
                    {
                        $sonuc = $uyeQ->fetch(PDO::FETCH_ASSOC);
                        $sonuc["status"] = "success";
                        echo Util::jsonPrettyPrint(json_encode($sonuc));
                    }
                    else
                    {
                        $this->errlog->insertWarn("Hatalı/Silinmiş Üye ID : ". $uyeID);
                        echo Util::jsonPrettyPrint("{\"status\": \"nosuchUye\"}");
                    }
                }
                catch(PDOException $exc)
                {
                     $this->errlog->insertErr("Üye sorgusu sırasında hata oluştu : ". $exc->getMessage());
                     echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                }
            }
            elseif($opcode["command"] == "editUye")
            {
                header("Content-type: application/json; charset=UTF-8");
                if(in_array("editUye", $this->permited_fields) ||  in_array("editSelfUye", $this->permited_fields))
                {
                    try
                    {
                        $uyeDecode = Util::tokenDecode($_POST["uyeId"]);
                        if($uyeDecode["command"] != "uyeId")
                        {
                            $this->errlog->insertWarn("Hatalı düzenleme tokeni : ". print_r($_POST, true) );
                            die(Util::jsonPrettyPrint("{\"status\": \"err\"}"));
                        }
                        $uyeID = $uyeDecode["values"];
                        $uyeQ = $this->pdoDB->prepare("SELECT * FROM uyeler WHERE id = :uyeId");
                        $uyeQ->execute(array("uyeId" => $uyeID));
                        if($uyeQ->rowCount() > 0)
                        {
                            if(!in_array("editUye", $this->permited_fields) &&  in_array("editSelfUye", $this->permited_fields))
                            {
                                $editingUye = $uyeQ->fetchObject();
                                if($editingUye->ekleyen_id != $_SESSION["userid"])
                                {
                                    $this->errlog->insertWarn("Üyeler modülünde yetkisiz düzenleme isteği yapıldı");
                                    die (Util::jsonPrettyPrint("{\"status\": \"notpermit\"}"));
                                }
                            }
                            $uyeDuzenQ = $this->pdoDB->prepare("UPDATE uyeler SET isim = :uyeIsim, bolum = :uyeBolum, cinsiyet = :uyeCinsiyet, eposta = :uyeEposta, telefon = :uyeTel, duzenlenme_tarihi = :duzTarih, ekleyen_id = :eklID WHERE id = :uyeId ");
                            $uyeDuzenQ->execute(array( "uyeId"       => $uyeID,
                                                       "uyeIsim"     => $_POST["uyeAdi"],
                                                       "uyeCinsiyet" => $_POST["uyeCinsiyet"], 
                                                       "uyeBolum"    => $_POST["uyeBolum"],
                                                       "uyeEposta"   => $_POST["uyeEPosta"],
                                                       "uyeTel"      => Util::textUnMask($_POST["uyeTel"],"(###) ### ####"), 
                                                       "duzTarih"    => date("Y-m-d H:i:s"),
                                                       "eklID"       => $_SESSION["userid"] ));
                            $pageN = $this->pdoDB->query("SELECT FLOOR(COUNT(*)/ $this->uyePerPage) as sayfa_no FROM uyeler WHERE id < $uyeID ORDER BY id")->fetchColumn();
                            echo Util::jsonPrettyPrint("{\"status\": \"success\", \"page\": $pageN }");
                        }
                        else
                        {
                            $this->errlog->insertWarn("Hatalı/Silinmiş Üye ID : ". $uyeID);
                            echo Util::jsonPrettyPrint("{\"status\": \"nosuchUye\"}");
                        }
                    }
                    catch(PDOException $exc)
                    {
                        $this->errlog->insertErr("Üye düzenleme sırasında hata oluştu : ". $exc->getMessage());
                        echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                    }
                }
                else
                {
                    $this->errlog->insertWarn("Üyeler modülünde yetkisiz düzenleme isteği yapıldı");
                    echo Util::jsonPrettyPrint("{\"status\": \"notpermit\"}");
                }
            }
            else if($opcode["command"] == "deleteUye")
            {
                header("Content-type: application/json; charset=UTF-8");
                if(in_array("deleteUye", $this->permited_fields))
                {
                    $uyeDecode = Util::tokenDecode($_POST["uyeId"]);
                    if($uyeDecode["command"] != "uyeId")
                    {
                        $this->errlog->insertWarn("Hatalı düzenleme tokeni : ". print_r($_POST, true) );
                        die(Util::jsonPrettyPrint("{\"status\": \"err\"}"));
                    }
                    $uyeID = $uyeDecode["values"];
                    try
                    {
                        $uyeQ = $this->pdoDB->prepare("SELECT isim, eposta, telefon FROM uyeler WHERE id = :uyeId");
                        $uyeQ->execute(array("uyeId" => $uyeID));
                        if($uyeQ->rowCount() > 0)
                        {
                            $pageN = $this->pdoDB->query("SELECT FLOOR(COUNT(*)/ $this->uyePerPage) as sayfa_no FROM uyeler WHERE id < $uyeID ORDER BY id")->fetchColumn();
                            $uyeDelQ = $this->pdoDB->prepare("DELETE FROM uyeler WHERE id = :uyeId");
                            $uyeDelQ->execute(array("uyeId" => $uyeID));
                            echo Util::jsonPrettyPrint("{\"status\": \"success\", \"page\": $pageN }");
                        }
                        else
                        {
                            $this->errlog->insertWarn("Hatalı/Silinmiş Üye ID : ". $uyeID);
                            echo Util::jsonPrettyPrint("{\"status\": \"nosuchUye\"}");
                        }                        
                    }
                    catch(PDOException $exc)
                    {
                        $this->errlog->insertErr("Üye silme sırasında hata oluştu : ". $exc->getMessage());
                        echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                    }
                }
                else
                {
                    $this->errlog->insertWarn("Üyeler modülünde yetkisiz silme isteği yapıldı");
                    echo Util::jsonPrettyPrint("{\"status\": \"notpermit\"}");
                }
            }
            else if($opcode["command"] == "searchUye")
            {
                header("Content-type: application/json; charset=UTF-8");
                if(!is_string($_POST["searchStr"]))
                {
                    die(Util::jsonPrettyPrint ("{\"status\": \"err\"}"));
                }
                $responseArr = array();
                $results = array();
                try
                {
                    $uyeSearchQ = $this->pdoDB->prepare("SELECT * FROM uyeler WHERE (isim LIKE :ISIM ) OR (eposta LIKE :EPOSTA ) OR ( telefon LIKE :TEL)");
                    $uyeSearchQ->execute(array(
                        "ISIM" => "%" . $_POST["searchStr"] . "%",
                        "EPOSTA" => "%" . $_POST["searchStr"] . "%",
                        "TEL" => "%" . $_POST["searchStr"]. "%",
                    ));
                    $responseArr["count"] = $uyeSearchQ->rowCount();
                    while($currentUye = $uyeSearchQ->fetch(PDO::FETCH_ASSOC))
                    {
                        array_push($results, $currentUye);
                    }
                    $responseArr["results"] = $results;
                    $responseArr["status"] = "success";
                    $response = json_encode($responseArr);
                    echo Util::jsonPrettyPrint($response);
                }
                catch (PDOException $exc)
                {
                    $this->errlog->insertErr("Üye arama sırasında hata oluştu : ". $exc->getMessage());
                    echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                }
            }
            else if($opcode["command"] == "searchUyeList")
            {
                $this->listUye($_POST["pagenum"], true, $_POST["searchStr"]);
            }
            else
            {
                header("Content-type: application/json; charset=UTF-8");
                $this->errlog->insertErr("Üyeler modülü hatalı işlem kodu : ". print_r($_POST, true));
                die(Util::jsonPrettyPrint("{\"status\": \"err\"}"));
            }
        }
    }
    
    private function listUye($page_n = 0, $search = false, $searchStr = "")
    {
        try
        {
            $uyeSQL = "";
            $executeArr = array();
            $uyeCountSQL = "";
            $executeCountArr = array();
            if($search && $searchStr != "")
            {
                $uyeSQL = "SELECT * FROM uyeler WHERE (isim LIKE :ISIM ) OR (eposta LIKE :EPOSTA ) OR ( telefon LIKE :TEL) ORDER BY isim LIMIT :page, :item_per_page";
                $uyeCountSQL = "SELECT COUNT(*) FROM uyeler WHERE (isim LIKE :ISIM ) OR (eposta LIKE :EPOSTA ) OR ( telefon LIKE :TEL) ORDER BY isim";
                $executeArr = array(
                        "ISIM" => "%" . $_POST["searchStr"] . "%",
                        "EPOSTA" => "%" . $_POST["searchStr"] . "%",
                        "TEL" => "%" . $_POST["searchStr"]. "%",
                        "page" => $page_n * $this->uyePerPage, "item_per_page" => $this->uyePerPage
                    );
                $executeCountArr = array(
                    "ISIM" => "%" . $_POST["searchStr"] . "%",
                    "EPOSTA" => "%" . $_POST["searchStr"] . "%",
                    "TEL" => "%" . $_POST["searchStr"]. "%",
                );
            }
            else
            {
                $uyeSQL = "SELECT * FROM uyeler ORDER BY isim LIMIT :page, :item_per_page";
                $uyeCountSQL = "SELECT COUNT(*) FROM uyeler ORDER BY isim";
                $executeArr = array("page" => $page_n * $this->uyePerPage, "item_per_page" => $this->uyePerPage);
            }
            
            $uyeQuery = $this->pdoDB->prepare($uyeSQL);
            $uyeQuery->execute($executeArr);
            $uyeCountQuery = $this->pdoDB->prepare($uyeCountSQL);
            $uyeCountQuery->execute($executeCountArr);
            
            $uyesayisi = $uyeCountQuery->fetchColumn();
            $last_page =  ceil($uyesayisi/$this->uyePerPage) - 1;
            if($page_n > $last_page)
                $page_n = $last_page;
?>
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Üyeler
                            <div class="pull-right">
                                <?php 
                                if(in_array("addUye", $this->permited_fields) )
                                { ?>
                                    <button class="btn btn-primary btn-xs" type="button" id="btnUyeEkle" data-job="addUye" data-toggle="modal" data-target="#modalUyeEkleDuz" title="Üye Ekle"><span class="glyphicon glyphicon-plus"></span> Üye Ekle</button>
                                <?php 
                                } ?>
                                <form name="uyeSearch" id="frmUyeSearch" role="form" class="form-inline">
                                    <div class="form-group">
                                        <div class="input-group input-group-sm">
                                            <input id="txtAraStr" type="text" class="form-control input-sm" placeholder="Ara.." <?php if($search) { ?> value="<?= $searchStr ?>" <?php } ?> />
                                            <span class="input-group-btn">
                                                <button id="btnAra" class="btn btn-primary glyphicons search" type="button" role="search"></button>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                                <button class="btn btn-success btn-xs" type="button" id="btnYenile" data-page="0" data-job="refresh" title="Yenile"><span class="glyphicon glyphicon-refresh"></span> Yenile</button>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <th class="col-xs-1" style="width: 4%; text-align: right;">#</th>
                                    <th class="col-xs-2">Üye Adı</th>
                                    <th class="col-xs-3">Bölüm</th>
                                    <th class="col-xs-2">E-Posta</th>
                                    <th class="col-xs-2">Telefon</th>
                                    <th class="col-xs-1">İşlemler</th>
                                </thead>
                                <tbody>
                                    <?php
                                    for($i = $page_n * $this->uyePerPage + 1; $currentUye =  $uyeQuery->fetch(PDO::FETCH_OBJ); $i++)
                                    {
                                    ?>
                                    <tr data-uyeid = "<?= urlencode(Util::tokenGenerate("uyeId", $currentUye->id ))?>">

                                        <td class="align-right"  style="text-align: right;"><?= $i ?></td>
                                        <td><span class="glyphicons <?php if($currentUye->cinsiyet == "E"){?>old_man<?php } else { ?>woman<?php } ?>" style="font-size: 18px;"></span><?= $currentUye->isim ?></td>
                                        <td><?= $currentUye->bolum ?></td>
                                        <td><?= $currentUye->eposta ?></td>
                                        <td><?= Util::textMask($currentUye->telefon, "0(###) ### ####"); ?></td>
                                        <td>
                                            <?php 
                                            if(in_array("editUye", $this->permited_fields) ||  (in_array("editSelfUye", $this->permited_fields) && $currentUye->ekleyen_id == $_SESSION["userid"]))
                                            { 
                                            ?>
                                                <button type="button" class="btn btn-xs btn-orange" title="Düzenle" data-job="editUye" data-toggle="modal" data-target="#modalUyeEkleDuz"><span class="glyphicon glyphicon-edit"></span></button>
                                            <?php 
                                            }
                                            if(in_array("deleteUye", $this->permited_fields) !== FALSE)
                                            {
                                            ?>
                                                <button type="button" class="btn btn-xs btn-danger" title="Sil" data-job="uyeSil" data-toggle="modal" data-target="#modalUyeSil"><span class="glyphicon glyphicon-trash"></span></button>
                                            <?php
                                            } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                        <div class="col-xs-4 col-xs-offset-4 text-center">
                            <ul class="pagination pagination-sm">
                                <li<?php if($page_n == 0){ ?> class="disabled"<?php }else { ?> onclick="$('#listArea').data('LoadPage')(<?= $page_n-1 ?>);"<?php } ?>><a href="javascript:void(0)">&laquo;</a></li>
                                <?php 
                                for($i=0; $i<$last_page+1; $i++){
                                ?>
                                <li<?php if($i == $page_n){?> class="active" <?php } else { ?> onclick="$('#listArea').data('LoadPage')(<?= $i ?>);"<?php } ?>><a><?= $i+1 ?></a></li>
                                    <?php }?>
                                <li<?php if($page_n == $last_page ){ ?> class="disabled"<?php }else { ?> onclick="$('#listArea').data('LoadPage')(<?= $page_n+1 ?>);"<?php } ?>><a href="javascript:void(0)">&raquo;</a></li> 
                            </ul>
                        </div>
                    </div><!-- panel-body -->
                </div><!-- panel-default -->
            </div><!-- liste-col -->
            <div class="col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">İstatistikler</h3></div>
                    <div class="panel-body">
                        <div class="container-fluid container-nopadding">
                            <div class="col-xs-12">
                                <h4 style="margin-top: 0;">Üye Sayısı</h4>
                                <?php
                                $cinsiyetQ = $this->pdoDB->query("SELECT COUNT(cinsiyet) as uyeSayisi, ROUND((COUNT(cinsiyet)/(SELECT COUNT(*) FROM uyeler)) * 100, 2) AS yuzde, cinsiyet FROM uyeler GROUP BY cinsiyet");
                                while($currentCinsiyet = $cinsiyetQ->fetchObject())
                                {
                                ?>
                                    <div class="col-xs-8"><?= $currentCinsiyet->cinsiyet == "E" ? "Erkek" : "Kadın" ?></div>
                                    <div class="col-xs-2"><?= $currentCinsiyet->uyeSayisi ?></div>
                                    <div class="col-xs-2">%<?= $currentCinsiyet->yuzde ?></div>
                                <?php
                                }
                                $toplamQ = $this->pdoDB->query("SELECT COUNT(*) FROM uyeler");
                                ?>
                                <div class="col-xs-8">Toplam</div>
                                <div class="col-xs-4"><?= $toplamQ->fetchColumn() ?></div>
                            </div>
                            <div class="col-xs-12">
                                <h4>Bölüm İstatistiği</h4>
                                <?php
                                $bolumQ = $this->pdoDB->query("SELECT COUNT(bolum) AS uyeSayisi, ROUND((COUNT(bolum)/(SELECT COUNT(*) FROM uyeler)) * 100, 2) AS yuzde, bolum FROM uyeler GROUP BY bolum ORDER BY uyeSayisi DESC");
                                while($currentBolum = $bolumQ->fetchObject())
                                {
                                ?>
                                    <div class="col-xs-8"><?= $currentBolum->bolum ?></div>
                                    <div class="col-xs-2"><?= $currentBolum->uyeSayisi ?></div>
                                    <div class="col-xs-2">%<?= $currentBolum->yuzde ?></div>
                                <?php
                                }
                                ?>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <script type="text/javascript">
                function RefreshEvents()
                {
                    $("#btnYenile").click(function(){
                        $('#listArea').data('LoadPage')($(this).attr("data-page"));
                    });
                    
                    $("#btnAra").click(function(){
                        $('#listArea').data('LoadPage')(0, true, $("#txtAraStr").val());
                    });
                    
                    $("#frmUyeSearch").submit(function() {
                        $("#btnAra").trigger("click");
                        return false;
                    });
                }
            </script>
<?php
        }
        catch(PDOException $exception)
        {
            $this->errlog->insertErr("Üye listesi alınamadı ".$exception->getMessage()." Satır: ".$exception->getLine());
?>
            <div class="alert alert-danger">Bir hata gerçekleşti</div>
<?php            
        }
        
    }

}

try 
{
    new UyelerOps($pdoDB,  $default_errlog);
} 
catch (\Exception $exc) 
{
?>
            <div class="alert alert-danger" id="alertModuleErr"><span class="glyphicon glyphicon-warning-sign"></span> <?= $exc->getMessage()?></div>
<?php
}

