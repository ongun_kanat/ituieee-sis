<?php 
use ituieee\lib\Util; ?>
<script type="text/javascript">
     
    $(document).ready(function(ev)
    {
        $.ajaxSetup({
            url: "<?= __MODULEDIR__?>Komiteler/operations.php"
        });
        
        $("#listArea").data({
            LoadPage: function(){
                $.ajax({
                    method: "post",
                    data:"opcode=<?= urlencode(Util::tokenGenerate("list", date("Y-m-d H:i:s")))?>",
                    success: function (data)
                    {
                        $("#listArea").html(data);
                        RefreshEvents();
                    }
                });
            }
        });
        
        
        function ShowAlertErr(message)
        {
            $("#alertArea").removeClass("alert-danger").removeClass("alert-warning");
            $("#alertArea").removeClass("hidden").addClass("alert-danger").html(message);
            window.setTimeout(function(){ $("#alertArea").addClass("hidden");  }, 4000);
            $("#listArea").data('LoadPage')();
            
        }
        
        function ShowAlertWarn(message)
        {
            $("#alertArea").removeClass("alert-danger").removeClass("alert-warning");
            $("#alertArea").removeClass("hidden").addClass("alert").addClass("alert-warning").html(message);
            window.setTimeout(function(){ $("#alertArea").addClass("hidden"); }, 4000);
            $("#listArea").data('LoadPage')();
        }
        
        
<?php
        if(in_array("addKomite", $this->permited_fields) ||  in_array("editKomite", $this->permited_fields) )
        {
?>        
            $("#frmKomiteAddEdit input").jqBootstrapValidation().on("blur focus change keyup",function(event_data){
                if(!$("#frmKomiteAddEdit input").jqBootstrapValidation("hasErrors"))
                {
                    $("#btnModalAddKomite").removeAttr("disabled");

                    var komiteDuzenlendi = false;
                    $("#frmKomiteAddEdit input").each(function(){
                        if($(this).attr("data-prevval") != $(this).val())
                            komiteDuzenlendi = true;
                    });

                    if(komiteDuzenlendi)
                       $("#btnModalEditKomite").removeAttr("disabled");
                    else
                       $("#btnModalEditKomite").attr("disabled",""); 
                }
                else
                {
                    $("#btnModalAddKomite").attr("disabled","");
                    $("#btnModalEditKomite").attr("disabled","");
                }
            });
        

            $("#modalKomiteAddEdit").on("show.bs.modal",function(event_data)
            {
                var job = $(event_data.relatedTarget).attr("data-job");
                $(this).attr("data-jobtype", job);

                if(job == "addKomite")
                {
                    $("#modalTitleAddEdit").html("Komite Ekle");
                    $("#btnModalAddKomite").removeClass("hidden");
                }
                else if(job == "editKomite")
                {
                    $("#modalTitleAddEdit").html("Komiteyi Düzenle");
                    $("#btnModalEditKomite").removeClass("hidden");
                    var komiteId = $(event_data.relatedTarget).parents("tr").attr("data-komiteid");
                    $("#btnModalEditKomite").attr("data-komiteid", komiteId);
                    $.ajax({
                        method: "post",
                        data: "komiteId=" + komiteId + "&opcode=<?= urlencode(Util::tokenGenerate("getKomiteInfo", date("Y-m-d H:i:s"))); ?>",
                        dataType: "json",
                        success: function(data){
                            if(data.status == "success")
                            {
                                $("#txtModalKomiteAdi").val(data.isim);

                                $("#txtModalKomiteAdi").attr("data-prevval", data.isim);
                            }
                            else if( data.status == "nosuchKomite" )
                            {
                                ShowAlertWarn("Seçtiğiniz komite ne yazık ki bulunamadı. Veritabanında kaldırılmış olabilir. Yeni Komite Listesi için listeyi yenileyebilirsiniz.");
                                $("#modalKomiteAddEdit").modal('hide');
                            }
                            else
                            {
                                ShowAlertErr("Komite düzenleme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                                $("#modalKomiteAddEdit").modal('hide');
                            }
                        },
                       error: function(){
                           ShowAlertErr("Komite düzenleme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                            $("#modalKomiteAddEdit").modal('hide');
                       }
                    });
                }
            });
            
            $("#modalKomiteAddEdit").on("hide.bs.modal",function(event_data)
            {
                $("#frmKomiteAddEdit .form-group").removeClass("has-success").removeClass("has-warning").removeClass("has-error");
                $("#frmKomiteAddEdit input").val("");
                $("#btnModalAddKomite").attr("disabled","").addClass("hidden");
                $("#btnModalEditKomite").attr("disabled","").addClass("hidden");
                $("#btnModalEditKomite").removeAttr("data-komiteid");
            });
    
            $("#btnModalAddKomite").click(function(click_evt)
            {
                $.ajax({
                    method: "post",
                    data: $("#frmKomiteAddEdit").serialize() +"&opcode=<?= urlencode(Util::tokenGenerate("addKomite", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            LoadPage();
                        }
                        else
                        {
                            ShowAlertErr("Komite ekleme işlemi sırasında bir hata oluştu");
                        }
                        $("#modalKomiteAddEdit").modal('hide');
                    },
                    error: function(){
                        ShowAlertErr("Komite ekleme işlemi sırasında bir hata oluştu");
                        $("#modalKomiteAddEdit").modal('hide');
                    }
                });
            });
        
            $("#btnModalEditKomite").click(function(click_evt){
                var komiteId = $("#btnModalEditKomite").attr("data-komiteid");
                $.ajax({
                    method: "post",
                    data: $("#frmKomiteAddEdit").serialize() + "&komiteId=" + komiteId + "&opcode=<?= urlencode(Util::tokenGenerate("editKomite", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            LoadPage();
                        }
                        else
                        {
                            ShowAlertErr("Komite Düzenleme esnasında bir hata ile karşılaşıldı");
                        }
                        $("#modalKomiteAddEdit").modal('hide');
                    },
                    error: function(){
                        ShowAlertErr("Komite Düzenleme esnasında bir hata ile karşılaşıldı");
                        $("#modalKomiteAddEdit").modal('hide');
                    }
                });
            });
<?php
        }
        if(in_array("deleteKomite", $this->permited_fields))
        {
?>
            $("#modalKomiteDel").on("show.bs.modal",function(event_data)
            {
                var komiteId = $(event_data.relatedTarget).parents("tr").attr("data-komiteid");
                $.ajax({
                    method: "post",
                    data: "komiteId=" + komiteId + "&opcode=<?= urlencode(Util::tokenGenerate("getKomiteInfo", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            $("#fldModalKomiteAdi").html(data.isim);
                            $("#btnModalDeleteKomite").attr("data-komiteid", komiteId);
                        }
                        else if( data.status == "nosuchKomite" )
                        {
                            ShowAlertWarn("Seçtiğiniz komite bulunamadı. Veritabanında kaldırılmış olabilir. Yeni Komite Listesi için listeyi üstteki düğme yardımıyla yenileyebilirsiniz.");
                            $("#modalKomiteDel").modal('hide');
                        }
                        else
                        {
                            ShowAlertErr("Komite silme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                            $("#modalKomiteDel").modal('hide');
                        }
                    },
                    error: function(){
                        ShowAlertErr("Komite silme diyaloğu açılamıyor, bir hata ile karşılaşıldı!");
                        $("#modalKomiteDel").modal('hide');
                    }
                });
            });

            $("#modalKomiteDel").on("hide.bs.modal",function(event_data)
            {
                $("#fldModalKomiteAdi").html("");
                $("#btnModalDeleteKomite").removeAttr("data-komiteid");
            });

            $("#btnModalDeleteKomite").click(function(evt){
               var komiteId = $(this).attr("data-komiteid");
               $.ajax({
                    method: "post",
                    data: "komiteId=" + komiteId + "&opcode=<?= urlencode(Util::tokenGenerate("deleteKomite", date("Y-m-d H:i:s"))); ?>",
                    dataType: "json",
                    success: function(data){
                        if(data.status == "success")
                        {
                            LoadPage();
                        }
                        else
                        {
                            ShowAlertErr("Komite silme işlemi esnasında bir hata ile karşılaşıldı!");
                        }
                        $("#modalKomiteDel").modal('hide');
                    },
                    error: function(){
                        ShowAlertErr("Komite silme işlemi esnasında bir hata ile karşılaşıldı!");
                        $("#modalKomiteDel").modal('hide');
                    }
                }); 
            });
<?php
        }
?>
        $("input[mask]").each(function()
        {
            $(this).setMask($(this).attr("mask"));
        });
        
        $("#listArea").data("LoadPage")();
    });
</script>