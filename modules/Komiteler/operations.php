<?php 
namespace ituieee\modules\Komiteler;
include "includes.php";
use ituieee\lib\Util;
use ituieee\lib\DbUtil;
use \PDO;

class KomitelerOps extends \ituieee\lib\Module
{
    function __construct($pdoObj, $errlog) 
    {
        parent::__construct("Komiteler", $pdoObj, $errlog);
        $this->DrawSessionSensetiveParts();
    }
    
    protected function permissionCheckOk()
    {
        
        if($_POST["opcode"] != "" )
        {
            $opcode = Util::tokenDecode($_POST["opcode"]);
            if($opcode["command"] == "list")
            {
                $komiteQuery = $this->pdoDB->query("SELECT id, isim FROM komiteler ORDER BY isim");
?>
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Komiteler
                            <div class="pull-right">
                                <?php 
                                if(in_array("addKomite", $this->permited_fields))
                                { ?>
                                    <button class="btn btn-primary btn-xs" type="button" id="btnAddKomite" data-job="addKomite" data-toggle="modal" data-target="#modalKomiteAddEdit" title="Komite Ekle"><span class="glyphicon glyphicon-plus"></span> Komite Ekle</button>
                                <?php 
                                } ?>
                                <button class="btn btn-success btn-xs" type="button" id="btnRefresh" data-job="refresh" title="Yenile"><span class="glyphicon glyphicon-refresh"></span> Yenile</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <th class="col-xs-1" style="width: 4%; text-align: right;">#</th>
                                    <th class="col-xs-8">Komite Adı</th>
                                    <th class="col-xs-1">İşlemler</th>
                                </thead>
                                <tbody>
                                    <?php
                                    for($i = 1; $currentKomite =  $komiteQuery->fetchObject(); $i++)
                                    {
                                    ?>
                                    <tr data-komiteid = "<?= urlencode(Util::tokenGenerate("komiteId", $currentKomite->id ))?>">

                                        <td class="align-right"  style="text-align: right;"><?= $i ?></td>
                                        <td><?= $currentKomite->isim ?></td>
                                        <td>
                                            <?php 
                                            if(in_array("editKomite", $this->permited_fields))
                                            { 
                                            ?>
                                                <button type="button" class="btn btn-xs btn-orange" title="Düzenle" data-job="editKomite" data-toggle="modal" data-target="#modalKomiteAddEdit"><span class="glyphicon glyphicon-edit"></span></button>
                                            <?php 
                                            }
                                            if(in_array("deleteKomite", $this->permited_fields) !== FALSE)
                                            {
                                            ?>
                                                <button type="button" class="btn btn-xs btn-danger" title="Sil" data-job="delKomite" data-toggle="modal" data-target="#modalKomiteDel"><span class="glyphicon glyphicon-trash"></span></button>
                                            <?php
                                            } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div><!-- table-responsive -->
                    </div><!-- panel-body -->
                </div><!-- panel-default -->
            </div><!-- liste-col -->
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">İstatistikler</h3></div>
                    <div class="panel-body"></div>
                </div>
            </div>
            
            <script type="text/javascript">
                function RefreshEvents()
                {
                    $("#btnRefresh").click(function(){
                        $('#listArea').data('LoadPage')();
                    });
                }
            </script>
<?php
            }
            elseif( $opcode["command"] == "getKomiteInfo")
            {
                header("Content-type: application/json; charset=UTF-8");
                try
                {                   
                    $komiteDecode = Util::tokenDecode($_POST["komiteId"]);
                    $komiteId = $komiteDecode["values"];
                    $komiteQ = $this->pdoDB->prepare("SELECT isim FROM komiteler WHERE id = :komiteId");
                    $komiteQ->execute(array("komiteId" => $komiteId));
                    if($komiteQ->rowCount() > 0)
                    {
                        $sonuc = $komiteQ->fetch(PDO::FETCH_ASSOC);
                        $sonuc["status"] = "success";
                        echo Util::jsonPrettyPrint(json_encode($sonuc));
                    }
                    else
                    {
                        $this->errlog->insertWarn("Hatalı/Silinmiş Komite ID : ". $uyeID);
                        echo Util::jsonPrettyPrint("{\"status\": \"nosuchKomite\"}");
                    }
                }
                catch(PDOException $exc)
                {
                     $this->errlog->insertErr("Komite sorgusu sırasında hata oluştu : ". $exc->getMessage());
                     echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                }
            }
            elseif($opcode["command"] == "addKomite")
            {
                
                header("Content-type: application/json; charset=UTF-8");
                if(in_array("addKomite", $this->permited_fields))
                {
                    try
                    {
                        $komiteID =DbUtil::idGenerate("komiteler", 2);
                        $komiteAddQ = $this->pdoDB->prepare("INSERT INTO komiteler VALUES (:komiteId, :komiteAdi)");
                        $komiteAddQ->execute(array("komiteId" => $komiteID, "komiteAdi" => $_POST["komiteAdi"]));
                        echo Util::jsonPrettyPrint("{ \"status\": \"success\"}");
                    }
                    catch(PDOException $exc)
                    {
                        $this->errlog->insertErr("Komite ekleme sırasında hata oluştu : ". $exc->getMessage());
                        echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                    }
                }
                else
                {
                    $this->errlog->insertWarn("Komiteler modülünde yetkisiz ekleme isteği yapıldı");
                    echo Util::jsonPrettyPrint("{\"status\": \"notpermit\"}");
                }
            }
            elseif($opcode["command"] == "editKomite")
            {
                header("Content-type: application/json; charset=UTF-8");
                if(in_array("editKomite", $this->permited_fields))
                {
                    try 
                    {
                        $komiteDecode = Util::tokenDecode($_POST["komiteId"]);
                        if($komiteDecode["command"] != "komiteId")
                        {
                            $this->errlog->insertWarn("Hatalı düzenleme tokeni : ". print_r($_POST, true) );
                            die(Util::jsonPrettyPrint("{\"status\": \"err\"}"));
                        }
                        $komiteID = $komiteDecode["values"];
                        $checkQ = $this->pdoDB->prepare("SELECT id FROM komiteler WHERE id = :komiteId");
                        $checkQ->execute(array("komiteId" => $komiteID));
                        if($checkQ->rowCount() > 0)
                        {
                            $komiteEditQ = $this->pdoDB->prepare("UPDATE komiteler SET isim = :komiteAdi  WHERE id = :komiteId ");
                            $komiteEditQ->execute(array("komiteId" => $komiteID, "komiteAdi" => $_POST["komiteAdi"]));
                            echo Util::jsonPrettyPrint("{\"status\": \"success\" }");
                        }
                        else
                        {
                            $this->errlog->insertWarn("Hatalı/Silinmiş Komite ID : ". $uyeID);
                            echo Util::jsonPrettyPrint("{\"status\": \"nosuchKomite\"}");
                        }

                    }
                    catch (PDOException $exc) 
                    {
                        $this->errlog->insertErr("Komite düzenleme sırasında hata oluştu : ". $exc->getMessage());
                        echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                    }
                }
                else
                {
                    $this->errlog->insertWarn("Komiteler modülünde yetkisiz düzenleme isteği yapıldı");
                    echo Util::jsonPrettyPrint("{\"status\": \"notpermit\"}");
                }
            }
            else if($opcode["command"] == "deleteKomite")
            {
                header("Content-type: application/json; charset=UTF-8");
                if(in_array("deleteKomite", $this->permited_fields))
                {
                    $komiteDecode = Util::tokenDecode($_POST["komiteId"]);
                    if($komiteDecode["command"] != "komiteId")
                    {
                        $this->errlog->insertWarn("Hatalı düzenleme tokeni : ". print_r($_POST, true) );
                        die(Util::jsonPrettyPrint("{\"status\": \"err\"}"));
                    }
                    $komiteID = $komiteDecode["values"];
                    try
                    {
                        $checkQ = $this->pdoDB->prepare("SELECT id FROM komiteler WHERE id = :komiteId");
                        $checkQ->execute(array("komiteId" => $komiteID));
                        if($checkQ->rowCount() > 0)
                        {
                            $uyeDelQ = $this->pdoDB->prepare("DELETE FROM komiteler WHERE id = :komiteId");
                            $uyeDelQ->execute(array("komiteId" => $komiteID));
                            echo Util::jsonPrettyPrint("{\"status\": \"success\" }");
                        }
                        else
                        {
                            $this->errlog->insertWarn("Hatalı/Silinmiş Komite ID : ". $uyeID);
                            echo Util::jsonPrettyPrint("{\"status\": \"nosuchKomite\"}");
                        }                        
                    }
                    catch(PDOException $exc)
                    {
                        $this->errlog->insertErr("Komite silme sırasında hata oluştu : ". $exc->getMessage());
                        echo Util::jsonPrettyPrint("{\"status\": \"err\"}");
                    }
                }
                else
                {
                    $this->errlog->insertWarn("Komiteler modülünde yetkisiz silme isteği yapıldı");
                    echo Util::jsonPrettyPrint("{\"status\": \"notpermit\"}");
                }
            }
            else
            {
                header("Content-type: application/json; charset=UTF-8");
                $this->errlog->insertErr("Komiteler modülü hatalı işlem kodu : ". print_r($_POST, true));
                die(Util::jsonPrettyPrint("{\"status\": \"err\"}"));
            }
        }
    }
}

new KomitelerOps($pdoDB, $default_errlog);