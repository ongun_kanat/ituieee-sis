<?php 
namespace ituieee\modules\Komiteler;
require_once "includes.php";
use \Exception;

class KomitelerMod extends \ituieee\lib\Module 
{
    function __construct($pdoObj, $errlog) 
    {
        parent::__construct("Komiteler", $pdoObj, $errlog);
    }

    
    public function DrawBody() 
    {
        $this->DrawSessionSensetiveParts();
    }
    
    protected function permissionCheckOk() 
    {
        include __MODULEDIR__."Komiteler/javascript.php";  
?>
        <div class="col-xs-12 alert hidden" id="alertArea"></div>
        <div class="container-fluid" id="listArea"></div>
<?php
        if(in_array("addKomite", $this->permited_fields) ||  in_array("editKomite", $this->permited_fields) )
        {
?>
            <div id="modalKomiteAddEdit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-editmode="">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalTitleAddEdit"></h4>
                        </div>
                        <div class="modal-body">

                            <form role="form" class="form-horizontal" id="frmKomiteAddEdit"  >
                                <div class="container-fluid">
                                    <div class="form-group">
                                        <div class="col-sm-3"><label for="txtModalKomiteAdi" class="control-label">Komite Adı</label></div>
                                        <div class="col-sm-9"><input type="text" class="form-control" id="txtModalKomiteAdi" placeholder="Komite Adı" name="komiteAdi" pattern="[A-ZÜĞİŞÇÖ][A-Za-zÜĞİŞÇÖüğışçö].+ [A-ZÜĞİŞÇÖ][A-Za-zÜĞİŞÇÖüğışçö].+" minlength="5" required /></div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-success hidden" id="btnModalAddKomite" disabled>Ekle</button>
                            <button type="button" class="btn btn-primary hidden" id="btnModalEditKomite" disabled>Bilgileri Kaydet</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">İptal</button>
                        </div>
                    </div>
                </div>
            </div>
<?php
        }
        
        if(in_array("deleteKomite", $this->permited_fields))
        {
?>
            <div id="modalKomiteDel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Komite Sil</h4>
                        </div>
                        <div class="modal-body">
                            <span id="fldModalKomiteAdi"></span> adlı komiteyi silmek istiyor musunuz? Bu işlem geri alınamaz
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" id="btnModalDeleteKomite">Sil</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">İptal</button>
                        </div>
                    </div>
                </div>
            </div>
<?php
        }
    }

}

$komiteler = new KomitelerMod($pdoDB, $default_errlog);
$komiteler->DrawBody();
