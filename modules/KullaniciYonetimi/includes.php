<?php
if(!defined("__MAINDIR__"))
    define("__MAINDIR__", "../../");
if(!defined("__LIBDIR__"))
    define("__LIBDIR__", __MAINDIR__."lib/");
if(!defined("__CONFDIR__"))
    define("__CONFDIR__", __MAINDIR__."conf/");
if(!defined("__MODULEDIR__"))
    define("__MODULEDIR__", __MAINDIR__."modules/");

require_once __MAINDIR__."includes.php";