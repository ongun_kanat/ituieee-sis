<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            url: "<?= __MODULEDIR__?>KullaniciYonetimi/operations.php",
            method: "POST"
        });
        
        $("#modullerArea").data({
            LoadModuller: function () {
                $.ajax({
                    data: "opcode=listModuller",
                    success: function(data)
                    {
                        $("#modullerArea").html(data);
                    }
                });
            }
        });
        
        $("#gruplarArea").data({
            LoadGruplar: function() {
                $.ajax({
                    data: "opcode=listGruplar",
                    success: function(data)
                    {
                        $("#gruplarArea").html(data);
                    }
                });
            }
        });
        
        $("#btnModulRefresh").click(function(){
            $("#modullerArea").data("LoadModuller")();
        });
        
        $("#btnGrupRefresh").click(function(){
            $("#gruplarArea").data("LoadGruplar")();
        });
        
        $("#modullerArea").data("LoadModuller")();
        $("#gruplarArea").data("LoadGruplar")();
    });
</script>