<?php
namespace ituieee\modules\KullaniciYonetimi;
require_once "includes.php";
use PDOException;
use ituieee\lib\DBUtil;

class KullaniciYonetimiOps extends \ituieee\lib\Module
{
    public function __construct($pdoObj, $errlog) {
        parent::__construct("KullaniciYonetimi", $pdoObj, $errlog);
        $this->DrawSessionSensetiveParts();
    }

    protected function permissionCheckOk() 
    {
        
        
        if( $_POST["opcode"] != "" )
        {
            if( $_POST["opcode"] == "listModuller" )
            {
                try 
                {
                    $modulListQ = $this->pdoDB->query("SELECT * FROM moduller");
?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th class="col-xs-1" style="width: 4%; text-align: right;">#</th>
                                    <th>Modül Adı</th>
                                    <th class="col-xs-2">İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                            for($i = 1; $currentModul = $modulListQ->fetchObject(); $i++)
                            {
                                if($_SESSION["userid"] == "NULL" || $this->id != $currentModul->id)
                                {
?>
                                    <tr>
                                        <td style="text-align: right;"><?= $i ?></td>
                                        <td><?= $currentModul->gosterilen_isim ?></td>
                                        <td>
                                            <?php
                                                if(in_array("setPermission", $this->permited_fields) !== FALSE)
                                                {
                                            ?>
                                                <button title="Yetkileri Düzenle" class="btn btn-xs btn-primary" type="button"><span class="glyphicon glyphicon-lock"></span> Yetkileri Düzenle</button>
                                            <?php
                                                }
                                            ?>
                                        </td>
                                    </tr>
<?php
                                }
                            }
?>
                        </tbody>
                    </table>
                </div>
<?php
                }
                catch (PDOException $exc) 
                {
                    $this->errlog->insertErr("Modül listesi alınamadı ".$exception->getMessage()." Satır: ".$exception->getLine());
?>
                    <div class="alert alert-danger">Bir hata gerçekleşti</div>
<?php  
                }
            } //listModules
            if($_POST["opcode"] == "listGruplar")
            {
                try 
                {
                    $grupListQ = $this->pdoDB->query("SELECT * FROM gruplar");
?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th class="col-xs-1" style="width: 4%; text-align: right;">#</th>
                                    <th>Modül Adı</th>
                                    <th class="col-xs-2">İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
                            for($i = 1; $currentGrup = $grupListQ->fetchObject(); $i++)
                            {
?>
                                <tr>
                                    <td style="text-align: right;"><?= $i ?></td>
                                    <td><?= $currentGrup->isim ?></td>
                                    <td>
                                        <?php
                                            if(in_array("editGrup", $this->permited_fields) !== FALSE)
                                            {
                                        ?>
                                            <button title="Düzenle" class="btn btn-xs btn-orange" type="button"><span class="glyphicon glyphicon-edit"></span> Düzenle</button>
                                        <?php
                                            }
                                        ?>
                                    </td>
                                </tr>
<?php
                            }
?>
                        </tbody>
                    </table>
                </div>
<?php
                }
                catch (PDOException $exc) 
                {
                    $this->errlog->insertErr("Grup listesi alınamadı ".$exception->getMessage()." Satır: ".$exception->getLine());
?>
                    <div class="alert alert-danger">Bir hata gerçekleşti</div>
<?php  
                }
            }//listGroups
        }   
    }

}

new KullaniciYonetimiOps($pdoDB, $default_errlog);