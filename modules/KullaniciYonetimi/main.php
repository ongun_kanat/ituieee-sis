<?php
namespace ituieee\modules\KullaniciYonetimi;
require_once "includes.php";
use PDOException;
use ituieee\lib\DBUtil;

class KullaniciYonetimi extends \ituieee\lib\Module
{
    public function __construct($pdoObj, $errlog) {
        parent::__construct("KullaniciYonetimi", $pdoObj, $errlog);
        $this->DrawSessionSensetiveParts();
    }

    protected function permissionCheckOk() 
    {
        require_once __MODULEDIR__."KullaniciYonetimi/javascript.php";
?>
        <div class="container-fluid">
            <div class="col-lg-6">
                <div class="panel panel-cyan">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Modüller
                            <div class="pull-right">
                                <button class="btn btn-xs btn-success" type="button" title="Modül Listesini Yenile" id="btnModulRefresh"><span class="glyphicon glyphicon-refresh"></span> Yenile</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" id="modullerArea"></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Kullanıcı Grupları
                            <div class="pull-right">
                                <?php
                                if(in_array("addGrup", $this->permited_fields) !== FALSE)
                                {
                                ?>
                                    <button class="btn btn-xs btn-primary" type="button"><span class="glyphicon glyphicon-plus"></span> Grup Ekle</button>
                                <?php 
                                }                                        
                                ?>
                                <button class="btn btn-xs btn-success" type="button" id="btnGrupRefresh"><span class="glyphicon glyphicon-refresh"></span> Yenile</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body" id="gruplarArea"></div>
                </div>
            </div>
        </div>
        
<?php
    }

}

new KullaniciYonetimi($pdoDB, $default_errlog);