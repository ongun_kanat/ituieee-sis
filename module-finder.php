<?php
namespace ituieee;
require_once "includes.php";
require_once __LIBDIR__."/php/EmptyModule.php";
use ituieee\lib\EmptyModule;
use Exception;

if($_SESSION['logged_in'] != "true" )    
{
?>
<script type="text/javascript">window.location.href="index.php"</script>
<?php
}
else
{
    if(is_dir(__MODULEDIR__) && is_readable(__MODULEDIR__))
    {
        $moduleDirs = scandir(__MODULEDIR__);
        $modulesRead = array();
        foreach ($moduleDirs as $current) 
        {
            if(preg_match("/^[^\.]+$/", $current))
            {
                if(is_dir(__MODULEDIR__.$current) && is_executable(__MODULEDIR__.$current))
                {
                    try
                    {
                        new EmptyModule($current, $pdoDB, $default_errlog);
                        array_push($modulesRead, $current);
                    }
                    catch (Exception $exc)
                    {
                        $default_errlog->insertErr("$current modülü için tanımlama bilgileri oluşturulamadı : ". $exc->getMessage());
                    }
                }
                else
                {
                    $default_errlog->insertErr("$current adlı modülün dizini okunabilir değil");
                }
            }
        }
        $moduleListQ = $pdoDB->query("SELECT id, isim FROM moduller");
        $dbFields = array();
        while( $currentmodule = $moduleListQ->fetchObject() )
        {
            array_push($dbFields, $currentmodule);
        }
        foreach ( $dbFields as $currentmodule )
        {
            if(!in_array($currentmodule->isim, $modulesRead))
            {
                $moduleDeleteQ = $pdoDB->prepare("DELETE FROM moduller WHERE id = :ID");
                $moduleDeleteQ->execute(array("ID" => $currentmodule->id));
            }
        }
        
    }
    else
    {
        $default_errlog->insertErr("Modüller dizini okunabilir değil!");
    }
    
}
