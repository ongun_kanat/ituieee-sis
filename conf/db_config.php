<?php
namespace ituieee\config\db;
use PDO, PDOException;
require_once __CONFDIR__."errlog.php";
require_once __LIBDIR__. "php/DBUtil.php";

const HOST   = "localhost";
const USER   = "ituieee";
const PASS   = "ieee123";
const DBNAME = "ituieee_sis";

$pdoDB;
try 
{
    $pdoDB = new PDO("mysql:host=". HOST . ";dbname=" . DBNAME, USER, PASS);
    $pdoDB->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $pdoDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    \ituieee\lib\DbUtil::init($pdoDB);
}
catch (PDOException $exception) 
{
    $default_errlog->insertErr("Database'e bağlantı başarısız : ". $exception->getMessage());
    die("Veritabanı bağlantısı başarısız");
}