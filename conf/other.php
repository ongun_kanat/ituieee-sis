<?php
namespace ituieee\config\other;

mb_language("Turkish");
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
date_default_timezone_set("Europe/Istanbul");