<?php
namespace ituieee\config;
require_once __CONFDIR__ . "session.php";
require_once __CONFDIR__ . "errlog.php";
require_once __CONFDIR__ . "db_config.php";
require_once __CONFDIR__ . "settings.php";
require_once __CONFDIR__ . "other.php";
