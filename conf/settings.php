<?php
namespace ituieee\config\settings;
const __SETTINGS_FILE__ = "settings.json";
static $settings = null;
try
{
    if(file_exists(__CONFDIR__.__SETTINGS_FILE__) && is_readable(__CONFDIR__.__SETTINGS_FILE__))
        $settings = json_decode(file_get_contents(__CONFDIR__.__SETTINGS_FILE__));
    else
        throw new \Exception("Ayarlar dosyası yok veya okunabilir değil");
} catch (\Exception $exception) {
    $default_errlog->insertErr("Ayarlar dosyası açılamadı : ". $exception->getMessage());
    header("Content-type: text/html; charset=utf-8");
    die("Ayarlar yüklenemedi");
}