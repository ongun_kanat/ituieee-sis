-- phpMyAdmin SQL Dump
-- version 4.3.13
-- http://www.phpmyadmin.net
--
-- Anamakine: localhost
-- Üretim Zamanı: 03 Nis 2015, 22:40:18
-- Sunucu sürümü: 10.0.17-MariaDB-log
-- PHP Sürümü: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


GRANT USAGE ON *.* TO 'ituieee'@'%' IDENTIFIED BY PASSWORD '*206706B269B65E4F561713050702B79AA4E6E093';
GRANT ALL PRIVILEGES ON `ituieee\_%`.* TO 'ituieee'@'%';

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Veritabanı: `ituieee_sis`
--
CREATE DATABASE IF NOT EXISTS `ituieee_sis` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ituieee_sis`;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `donemler`
--

CREATE TABLE IF NOT EXISTS `donemler` (
  `id` int(11) NOT NULL,
  `isim` varchar(45) NOT NULL,
  `baslangic_tarihi` date NOT NULL,
  `bitis_tarihi` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `donemler`
--

INSERT INTO `donemler` (`id`, `isim`, `baslangic_tarihi`, `bitis_tarihi`) VALUES
(672663, '2014-2015', '2014-09-08', '2015-05-31');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `egitimkayitlari`
--

CREATE TABLE IF NOT EXISTS `egitimkayitlari` (
  `id` int(11) NOT NULL,
  `egitim_id` int(11) DEFAULT NULL,
  `uye_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `egitimler`
--

CREATE TABLE IF NOT EXISTS `egitimler` (
  `id` int(11) NOT NULL,
  `isim` varchar(150) NOT NULL,
  `uye_id` int(11) NOT NULL,
  `baslangic_tarihi` date NOT NULL,
  `bitis_tarihi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `gruplar`
--

CREATE TABLE IF NOT EXISTS `gruplar` (
  `id` int(11) NOT NULL,
  `isim` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `gruplar`
--

INSERT INTO `gruplar` (`id`, `isim`) VALUES
(57182932, 'Yönetim Kurulu'),
(97980795, 'Eğitmenler');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `grupuyelikleri`
--

CREATE TABLE IF NOT EXISTS `grupuyelikleri` (
  `id` int(11) NOT NULL,
  `grup_id` int(11) NOT NULL,
  `uye_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Üyelerin hangi gruba üye olduklarını tutan tablo';

--
-- Tablo döküm verisi `grupuyelikleri`
--

INSERT INTO `grupuyelikleri` (`id`, `grup_id`, `uye_id`) VALUES
(1473, 97980795, 6648693),
(6869, 57182932, 12345),
(8629, 97980795, 12345);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `komiteler`
--

CREATE TABLE IF NOT EXISTS `komiteler` (
  `id` int(11) NOT NULL,
  `isim` varchar(150) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `eklendigi_donem` int(11) NOT NULL,
  `aktif_donem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `komiteler`
--

INSERT INTO `komiteler` (`id`, `isim`, `eklendigi_donem`, `aktif_donem`) VALUES
(1, 'Women In Engineering', 672663, 672663),
(2, 'Laboratuvar Komitesi', 672663, 672663),
(7, 'Communications Society', 672663, 672663),
(45, 'Basın Yayın ve Medya Komitesi', 672663, 672663),
(85, 'EESTEC LC Istanbul', 672663, 672663),
(98, 'Computer Society', 672663, 672663);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `moduller`
--

CREATE TABLE IF NOT EXISTS `moduller` (
  `id` int(11) NOT NULL,
  `isim` varchar(100) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `gosterilen_isim` varchar(150) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `aciklama` text CHARACTER SET utf8 COLLATE utf8_turkish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Modül yönetim tablosu';

--
-- Tablo döküm verisi `moduller`
--

INSERT INTO `moduller` (`id`, `isim`, `gosterilen_isim`, `uid`, `gid`, `aciklama`) VALUES
(5839568, 'Uyeler', 'Üyeler', NULL, NULL, 'Her türlü üye işlemi'),
(10802386, 'Komiteler', 'Komiteler', NULL, NULL, 'Komite ekleme/düzenleme işlemleri'),
(91341596, 'KullaniciYonetimi', 'Kullanıcı ve Yetkiler', 12345, NULL, 'Kullanıcıların ve erişilebilecekleri modülleri düzenleyin');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `toplantilar`
--

CREATE TABLE IF NOT EXISTS `toplantilar` (
  `id` int(11) NOT NULL,
  `komite_id` int(11) NOT NULL,
  `tarih` date NOT NULL,
  `donem_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `uyeler`
--

CREATE TABLE IF NOT EXISTS `uyeler` (
  `id` int(11) NOT NULL,
  `isim` varchar(150) NOT NULL,
  `bolum` varchar(150) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `cinsiyet` enum('E','K') NOT NULL,
  `eposta` varchar(318) NOT NULL,
  `telefon` varchar(10) NOT NULL,
  `sifre` varchar(256) DEFAULT NULL,
  `grup_id` int(11) DEFAULT NULL COMMENT 'Bu üye bir yetki grubuna üye ise o id alınır aksi halde NULL değeri alır.',
  `ekleyen_id` int(11) DEFAULT NULL COMMENT 'Üyeyi ekleyen üyenin idsi',
  `eklenme_tarihi` datetime NOT NULL,
  `duzenlenme_tarihi` datetime NOT NULL,
  `eklendigi_donem` int(11) NOT NULL,
  `aktif_donem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `uyeler`
--

INSERT INTO `uyeler` (`id`, `isim`, `bolum`, `cinsiyet`, `eposta`, `telefon`, `sifre`, `grup_id`, `ekleyen_id`, `eklenme_tarihi`, `duzenlenme_tarihi`, `eklendigi_donem`, `aktif_donem`) VALUES
(12345, 'Sedat Öztürk', 'Geomatik Mühendisliği', 'E', 'sedatozturke@gmail.com', '5346160880', 'eea9fc592279b51994cf5e8043a6b33a9446e14b06e78ec7858666c47f03ca88', NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663),
(6648693, 'Besim Ongun Kanat', 'Bilgisayar Mühendisliği', 'E', 'ongun.kanat@gmail.com', '5068647493', NULL, NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663),
(9874654, 'Mustafa Yıldız', 'Metalurji ve Malzeme Mühendisliği', 'E', 'yildizmusta@itu.edu.tr', '5316269554', NULL, NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663),
(14713990, 'Mehmet Taner Ünal', 'Bilgisayar Mühendisliği', 'E', 'mtnr42@hotmail.com', '5556947460', NULL, NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663),
(17815230, 'Ozan Özyeğen', 'Bilgisayar Mühendisliği', 'E', 'ozan.ozyegen@gmail.com', '5354791929', NULL, NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663),
(32285938, 'Hakan Kavrazlı', 'Bilgisayar Mühendisliği', 'E', 'kavrazli.hakan@gmail.com', '5057335461', NULL, NULL, NULL, '2014-11-15 18:35:33', '2014-11-15 18:35:33', 672663, 672663),
(36598161, 'Serel Başak Kök', 'Tekstil Mühendisliği', 'K', 'marvenerel@gmail.com', '5076606840', NULL, NULL, NULL, '2014-09-28 01:01:47', '2014-09-28 01:01:47', 672663, 672663),
(38870032, 'Damla Sevim', 'Elektronik ve Haberleşme Mühendisliği', 'K', 'damlasevimm@gmail.com', '5344343579', NULL, NULL, NULL, '2014-09-28 00:59:00', '2014-09-28 00:59:00', 672663, 672663),
(43571595, 'İpek Kuvvetli', 'Elektrik Mühendisliği', 'K', 'ipekkuvvetli@gmail.com', '5076384797', NULL, NULL, NULL, '2014-09-28 00:53:33', '2014-09-28 00:53:33', 672663, 672663),
(46091123, 'Muhammed Aziz Ulak', 'Bilgisayar Mühendisliği', 'E', 'mazula95@gmail.com', '5515529688', NULL, NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663),
(56769752, 'Gizem Yağmur Kılıç', 'Elektrik Mühendisliği', 'K', 'gyagmurk@gmail.com', '5067624893', NULL, NULL, NULL, '2014-09-29 14:44:57', '2014-09-29 14:44:57', 672663, 672663),
(64921527, 'Faruk Yazıcı', 'Bilgisayar Mühendisliği', 'E', 'farukyazici82@gmail.com', '5544396480', NULL, NULL, NULL, '2014-09-28 00:57:49', '2014-09-28 00:57:49', 672663, 672663),
(71379218, 'Sema Karakaş', 'Bilgisayar Mühendisliği', 'K', 'krkssm@gmail.com', '5428119531', NULL, NULL, NULL, '2014-08-08 00:00:00', '2014-08-08 00:00:00', 672663, 672663);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yetkialanlari`
--

CREATE TABLE IF NOT EXISTS `yetkialanlari` (
  `id` int(11) NOT NULL,
  `isim` varchar(100) NOT NULL,
  `gosterilen_isim` varchar(150) CHARACTER SET utf8 COLLATE utf8_turkish_ci NOT NULL,
  `modul_id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `aciklama` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `yetkialanlari`
--

INSERT INTO `yetkialanlari` (`id`, `isim`, `gosterilen_isim`, `modul_id`, `uid`, `gid`, `aciklama`) VALUES
(10149382, 'editSelfUye', 'Kendisine Ait Üyeyi Değiştirme', 5839568, NULL, NULL, 'Normalde genel düzenleme yetkisi olmayan kullanıcılara kendilerinin girdiği üyeyi değiştirme imkanı tanır'),
(19463241, 'addGrup', 'Grup Ekle', 91341596, NULL, NULL, 'Kullanıcı Grubu Ekler'),
(42560246, 'editKomite', 'Komite Düzenleme', 10802386, NULL, NULL, NULL),
(49192060, 'editGrup', 'Grup Düzenleme', 91341596, NULL, NULL, 'Seçili kullanıcı grubunu düzenler'),
(52666130, 'deleteUye', 'Üye Silme', 5839568, NULL, NULL, NULL),
(53627674, 'editUye', 'Üye Düzenleme', 5839568, NULL, NULL, NULL),
(69935937, 'deleteGrup', 'Grup Sil', 91341596, NULL, NULL, 'Seçili kullanıcı grubunu siler'),
(73949043, 'setPermission', 'Modül İzinleri', 91341596, NULL, NULL, 'Bir modülün izinlerini değiştirir'),
(82774666, 'deleteSelfUye', 'Kendisine Ait Üyeyi Silme', 5839568, NULL, NULL, 'Normalde genel düzenleme yetkisi olmayan kullanıcılara kendilerinin girdiği üyeyi silme imkanı tanır'),
(86905766, 'addKomite', 'Komite Ekleme', 10802386, NULL, NULL, NULL),
(87773977, 'addUye', 'Üye Ekleme', 5839568, NULL, NULL, NULL),
(97039479, 'deleteKomite', 'Komite Silme', 10802386, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `yoklamalar`
--

CREATE TABLE IF NOT EXISTS `yoklamalar` (
  `id` int(11) NOT NULL,
  `toplanti_id` int(11) NOT NULL,
  `uye_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `donemler`
--
ALTER TABLE `donemler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `egitimkayitlari`
--
ALTER TABLE `egitimkayitlari`
  ADD PRIMARY KEY (`id`), ADD KEY `EGITIMKAYITLAR_UYEID` (`uye_id`), ADD KEY `EGITIMKAYITLAR_EGITIMID` (`egitim_id`);

--
-- Tablo için indeksler `egitimler`
--
ALTER TABLE `egitimler`
  ADD PRIMARY KEY (`id`), ADD KEY `EGITIMLER_UYEID` (`uye_id`);

--
-- Tablo için indeksler `gruplar`
--
ALTER TABLE `gruplar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `grupuyelikleri`
--
ALTER TABLE `grupuyelikleri`
  ADD PRIMARY KEY (`id`), ADD KEY `grup_id` (`grup_id`), ADD KEY `uye_id` (`uye_id`);

--
-- Tablo için indeksler `komiteler`
--
ALTER TABLE `komiteler`
  ADD PRIMARY KEY (`id`), ADD KEY `KOMITELER_EKLENDIGIDONEM` (`eklendigi_donem`), ADD KEY `KOMITELER_AKTIFDONEM` (`aktif_donem`);

--
-- Tablo için indeksler `moduller`
--
ALTER TABLE `moduller`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQUENESS` (`id`,`isim`), ADD KEY `UID` (`uid`), ADD KEY `GID` (`gid`);

--
-- Tablo için indeksler `toplantilar`
--
ALTER TABLE `toplantilar`
  ADD PRIMARY KEY (`id`), ADD KEY `TOPLANTILAR_KOMITEID` (`komite_id`), ADD KEY `TOPLANTILAR_DONEMID` (`donem_id`), ADD KEY `TARIH` (`tarih`);

--
-- Tablo için indeksler `uyeler`
--
ALTER TABLE `uyeler`
  ADD PRIMARY KEY (`id`), ADD KEY `UYELER_GRUPID` (`grup_id`), ADD KEY `UYELER_EKLEYENID` (`ekleyen_id`), ADD KEY `UYELER_AKTIFDONEM` (`aktif_donem`), ADD KEY `UYELER_EKLENDIGIDONEM` (`eklendigi_donem`);

--
-- Tablo için indeksler `yetkialanlari`
--
ALTER TABLE `yetkialanlari`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQUENESS` (`modul_id`,`isim`), ADD KEY `YETKILER_MODULID` (`modul_id`), ADD KEY `YETKILER_UYEID` (`uid`), ADD KEY `YETKILER_GRUPID` (`gid`);

--
-- Tablo için indeksler `yoklamalar`
--
ALTER TABLE `yoklamalar`
  ADD PRIMARY KEY (`id`), ADD KEY `TOPLANTIID` (`toplanti_id`), ADD KEY `UYEID` (`uye_id`);

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `egitimkayitlari`
--
ALTER TABLE `egitimkayitlari`
ADD CONSTRAINT `EGITIMKAYITLAR_EGITIMID` FOREIGN KEY (`egitim_id`) REFERENCES `egitimler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `EGITIMKAYITLAR_UYEID` FOREIGN KEY (`uye_id`) REFERENCES `uyeler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `egitimler`
--
ALTER TABLE `egitimler`
ADD CONSTRAINT `EGITIMLER_UYEID` FOREIGN KEY (`uye_id`) REFERENCES `uyeler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `grupuyelikleri`
--
ALTER TABLE `grupuyelikleri`
ADD CONSTRAINT `GRUPUYELIKLERI_GID` FOREIGN KEY (`grup_id`) REFERENCES `gruplar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `GRUPUYELIKLERI_UID` FOREIGN KEY (`uye_id`) REFERENCES `uyeler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `komiteler`
--
ALTER TABLE `komiteler`
ADD CONSTRAINT `KOMITELER_AKTIFDONEM` FOREIGN KEY (`aktif_donem`) REFERENCES `donemler` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `KOMITELER_EKLENDIGIDONEM` FOREIGN KEY (`eklendigi_donem`) REFERENCES `donemler` (`id`) ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `moduller`
--
ALTER TABLE `moduller`
ADD CONSTRAINT `MODULLER_GRUPID` FOREIGN KEY (`gid`) REFERENCES `gruplar` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `MODULLER_UYEID` FOREIGN KEY (`uid`) REFERENCES `uyeler` (`id`) ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `toplantilar`
--
ALTER TABLE `toplantilar`
ADD CONSTRAINT `TOPLANTILAR_DONEMID` FOREIGN KEY (`donem_id`) REFERENCES `donemler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `TOPLANTILAR_KOMITEID` FOREIGN KEY (`komite_id`) REFERENCES `komiteler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `uyeler`
--
ALTER TABLE `uyeler`
ADD CONSTRAINT `UYELER_AKTIFDONEM` FOREIGN KEY (`aktif_donem`) REFERENCES `donemler` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `UYELER_EKLENDIGIDONEM` FOREIGN KEY (`eklendigi_donem`) REFERENCES `donemler` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `UYELER_EKLEYENID` FOREIGN KEY (`ekleyen_id`) REFERENCES `uyeler` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `UYELER_GRUPID` FOREIGN KEY (`grup_id`) REFERENCES `gruplar` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `yetkialanlari`
--
ALTER TABLE `yetkialanlari`
ADD CONSTRAINT `YETKILER_GRUPID` FOREIGN KEY (`gid`) REFERENCES `gruplar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `YETKILER_MODULID` FOREIGN KEY (`modul_id`) REFERENCES `moduller` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `YETKILER_UYEID` FOREIGN KEY (`uid`) REFERENCES `uyeler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Tablo kısıtlamaları `yoklamalar`
--
ALTER TABLE `yoklamalar`
ADD CONSTRAINT `YOKLAMALAR_TOPLANTIID` FOREIGN KEY (`toplanti_id`) REFERENCES `toplantilar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `YOKLAMALAR_UYEID` FOREIGN KEY (`uye_id`) REFERENCES `uyeler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
