<?php
namespace ituieee;
use ituieee\lib\Page;
require_once "includes.php";

class IndexPage extends Page
{

    public function __construct()
    {
        parent::__construct();
        $this->DrawHeader();
        $this->DrawBody();
        $this->DrawFooter();
    }
    public function DrawBody()
    {
        $this->DrawSessionSensetiveParts();
    }
    public function DrawHeader() 
    {
?>
        <!DOCTYPE html>
        <html lang="tr">
        <head>
            <meta charset="UTF-8" />
            <link href="styles/main.css" rel="stylesheet" type="text/css"/>
            <?php require_once "js_includes.php"; ?>
            <title>IEEE SiS</title>
            <script type="text/javascript">
                $(window).ajaxStart(function(){
                    $("#ajaxLoadingBar").removeClass("hidden");
                });
                $(window).ajaxStop(function(){
                    $("#ajaxLoadingBar").addClass("hidden");
                });
                $(document).ready(function(){
                    
                });
            </script>
        </head>
        <body>
<?php
    }
    public function DrawFooter() 
    {
?>
            
        </body>
        </html>
<?php
    }
    protected function sessionCheckFail()
    {
?>
        <nav class="navbar navbar-default" role="navigation" style="border-radius: 0;">
            <div class="container-fluid">
                <div class="navbar-header">               
                    <a class="brand"><img src="images/ituieee_beyaz_kucuk.png" style="margin-top: 5px;" /></a>
                </div>            
            </div>
        </nav>
        <div class="container-fluid">
            <div class="modal-dialog modal-sm hidden" id="ajaxLoadingBar">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <div class="container-fluid text-center">
                            <span><img src="images/loader.gif" /></span>
                            <span style="font-size: 18px;">Yükleniyor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "login.php" ?>
<?php
    }
    
    protected function sessionCheckOk() 
    {
?>
        <nav class="navbar navbar-default" role="navigation" style="border-radius: 0;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target="#ust_erisim">
                        <span class="sr-only">Site Navigasyonu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="brand"><img src="images/ituieee_beyaz_kucuk.png" style="margin-top: 5px;" /></a>
                </div>
                <div class="collapse navbar-collapse" id="ust_erisim">
                    <ul class="nav navbar-nav">
                        <li><a href="javascript:void(0);" data-moduleload="<?= $GLOBALS["settings"]->ana_modul ?>">Ana Sayfa</a></li>
<?php
                        /* @var $GLOBALS['pdoDB'] \PDO  */
                        $modulePermCheck;
                        if($_SESSION["userid"] == "NULL")
                        {
                            $modulePermCheck = $GLOBALS["pdoDB"]->query("SELECT isim, gosterilen_isim  FROM moduller");
                        }
                        else
                        {
                            $modulePermCheck = $GLOBALS["pdoDB"]->prepare("SELECT isim, gosterilen_isim FROM moduller WHERE gid = ANY (SELECT grup_id FROM grupuyelikleri WHERE uye_id = :uyeID) OR uid = :UID");
                            $modulePermCheck->execute(array("uyeID" => $_SESSION["userid"], "UID" => $_SESSION["userid"]));
                        }
                        
                        while ( $currentModule = $modulePermCheck->fetchObject() )
                        {
?>
                        <li><a href="javascript:void(0);" data-moduleload="<?= $currentModule->isim ?>"><?= $currentModule->gosterilen_isim ?></a></li>
<?php
                        }
?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="logout.php"><span class="glyphicons log_out" style="font-size: 25px;"></span>Oturumu Kapat</a></li>
                    </ul>
                </div>
                
            </div>
        </nav>
        <div class="container-fluid">
            <div class="modal-dialog modal-sm hidden" id="ajaxLoadingBar">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <div class="container-fluid text-center">
                            <span><img src="images/loader.gif" /></span>
                            <span style="font-size: 18px;">Yükleniyor</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("a[data-moduleload]").click(function(){
                    ModuleMgt.LoadModule($(this).attr("data-moduleload"));
                });
                ModuleMgt.LoadModule("<?= $GLOBALS['settings']->ana_modul?>");
            });
            
        </script>
        <div class="container-fluid" id="paneModuleArea"></div>
<?php
    }
    
}

$page = new IndexPage($pdoDB, $default_errlog);
