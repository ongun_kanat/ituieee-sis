<?php
namespace ituieee;
require_once "includes.php";

if($_SESSION['logged_in'] != "true" )    
{
?>
<script type="text/javascript">window.location.href="index.php"</script>
<?php
}
else
{
    if($_POST["loadmdl"] == "true" && $_POST["modname"]!="" && file_exists("./modules/".$_POST["modname"]."/main.php"))
    {
        require_once  "./modules/".$_POST["modname"]."/main.php";
    }
    else
    {
?>
        <div class="alert alert-danger" id="alertModuleErr"><span class="glyphicon glyphicon-warning-sign"></span> Geçersiz modül isteği yapıldı</div>
<?php
    }
}?>
